//EmulaEEprom.c

#include "EmulaEEprom.h"
#include "derivative.h" /* include peripheral declarations */

#define EEpromStartAddr 0x4000

void InitFlash(void)
{
    (void)FLASH_IAPSR;
    FLASH_CR1_FIX=0;//Standard programming time of 1/2 tprog
}

void WriteEEprom(U16 Addr,U8 Valor)
{
    __disable_interrupt();
    __RESET_WATCHDOG();
    //enabling write access to the DATA area
    FLASH_DUKR=0xAE;//First hardware MASS key
    FLASH_DUKR=0x56;//Second hardware MASS key
    while(!FLASH_IAPSR_DUL);
    *(__near U8*)(EEpromStartAddr+Addr)=Valor;
    while(!FLASH_IAPSR_EOP);
    //disable write access
    FLASH_IAPSR_DUL=0;
    __enable_interrupt();
}

U8 ReadEEprom(U16 Addr)
{
    U8 Valor;

    Valor=*(__near U8*)(EEpromStartAddr+Addr);
    return(Valor);
}

void WriteEEpromInt(U16 Addr, U16 Valor)
{
    WriteEEprom(Addr+1,(U8)Valor);
    Valor>>=8;
    WriteEEprom(Addr,(U8)Valor);
}

U16 ReadEEpromInt(U16 Addr)
{
    U16 Valor;

    Valor=ReadEEprom(Addr);
    Valor<<=8;    
    Valor|=ReadEEprom(Addr+1);
    return(Valor);
}
