#ifndef COP_PROCESS_H
#define COP_PROCESS_H

#include "Tipos_v2.h"           /* definição dos tipos de variáveis                 */

/*
    //COP a cada 10ms na interrupção
    (void)InstalaRotina(&CopProcess,10,MODO_INTERRUPT);
*/
void CopProcess(void);
void ResetCpu(void);

/*
    //tarefa no loop principal a cada 1 seg
    (void)InstalaRotina(&CopPeriodic,1000,MODO_LOOP_TEMPO);
*/
void CopPeriodic(void);

#endif
//fim do arquivo
