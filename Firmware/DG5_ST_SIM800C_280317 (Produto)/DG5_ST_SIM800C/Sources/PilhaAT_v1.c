//Pilha_AT.c
//Joel Langer
//Ultima Reviz�o 06/02/2009
//Vers�o 1
//A pilha � �nica para m�do mestre e escravo

#include "PilhaAT_v1.h"         /* processador de comandos AT                       */
#include "derivative.h" //include peripheral declarations
#include "Serial232_v2.h"//fun��es de comunica��o serial

CAR8 BufSerial[TamanDoBuf];             //buffer para entrada dos comandos seriais
U8 ApontBuf;                            //apontador indexador do buffer serial

CAR8 BufArgs[TamanDoBufArgs];           //buffer para entrada dos comandos seriais
U8 ApontBufArs;                         //apontador indexador do buffer de argumentos

U8 NumComands;                          //n�mero de comandos instalados no sistema
U8 ComandValid;                         //informa qual comando foi encontrado e deve ser tratado

TipComandos * ComandosSist[MaxComds];   //armazena o endere�o das rotinas
CAR8 * ArgumentComand[MaxComds];        //armazena o endere�o argumento de entrada

//inicializa��o do terminal at
void InitTerminal(void)
{
    BufSerial[TamanDoBuf-1]='\0';       //coloca o terminador na ultima posi��o do buffer
    ApontBuf=0;                         //zera o apontador do buffer
    NumComands=0;                       //nenhum comando instalado
}

//adiciona um comando a lista de comandos do terminal
//apontador para o endere�o do nome do comando e para o endere�o da rotina a ser chamada
U8 TermInsertComand(CAR8 * Arg,TipComandos * Comando)
{
    if (NumComands>=MaxComds)//caso a lista de comandos esteja cheia
    {
        return 0;//retorna 0 se a pilha de comandos est� cheia
    }
    //caso a pilha n�o esteja cheia
    ComandosSist[NumComands]=Comando;//adiciona o comando a lista de comandos
    ArgumentComand[NumComands]=Arg;//adiciona o parametro a lista de comandos
    NumComands++;//incrementa a pilha
    return 1;//retorna 1 se n�o ocorreu erro
}//fim do add comando

//remove um comando da lista de comandos do terminal
U8 TermDelComand(CAR8 * Arg)
{
    U8 Aux;//para os loops
    for(Aux=0;Aux<NumComands;Aux++)//enquanto for menor que a quantidade de comandos
    {
        if(ArgumentComand[Aux]==Arg)//verifica se o apontador armazenado na lista de comandos � igual ao que se quer remover
        {
            while(Aux<NumComands-1)//enquanto o auxiliar for menor que o ultimo comando -1
            {
                ComandosSist[Aux]=ComandosSist[Aux+1];//coloca o comando superior pra baixo na pilha
                ArgumentComand[Aux]=ArgumentComand[Aux+1];//coloca o argumento para baixo tamb�m
                Aux++;//incrementa o auxiliar
            }
            ComandosSist[Aux]=0;//apara o endere�o agora vaziu da pilha colocando nulo
            ArgumentComand[Aux]=0;
            NumComands--;//decrementa um comando da pilha
            return 1;//reorna 1 se foi possivel remover o comando
        }//n�o � igual
    }//fim do for
    return 0;//caso ocorra erro retorna 0
}//fim do del comando

//remove todos os comandos instalados no terminal
void TermClearAllAtCommands(void)
{
	U8 Aux;//para os loops
	for(Aux=0;Aux<NumComands;Aux++)//enquanto for menor que a quantidade de comandos
	{
		ComandosSist[Aux]=0;//apara o endere�o agora vaziu da pilha colocando nulo
		ArgumentComand[Aux]=0;
	}//fim do for
	NumComands=0;//zera a quantidade de comandos instalados
}//fim do del comando

//compara a matriz a com a b retorna 1 se igual 0 se n�o
U8 CompString(CAR8 *a,CAR8 *b)
{
    U8 Aux=0;//auxiliar para procurar por toda string    
    do
    {
        if (a[Aux] != b[Aux])//caso encontre um caracter diferente
        {
            return(0);//retorna como negativo
        }
        Aux++;//incrementa o apontador
    } while(a[Aux] != '\0' && b[Aux] != '\0');//permanece no loop enquanto a ou b n�o conter /0
    return((U8)(a[Aux]==b[Aux] ? 1 : 0));//caso seja igual o ultimo byte retorna 1
}

//trata os comandos da interrup��o
void TerminalTrataComandosInt(void)
{
    //Criamos um ponteiro para uma fun��o void
    TipComandos * Rotina;
    //carregamos o ponteiro com o endere�o da rotina
    Rotina=ComandosSist[ComandValid];
    //chama a rotina passando os argumentos
    Rotina(BufArgs,&ApontBufArs);
    //apaga o flag que informa que tem um comando pendente para ser tratado
}

//Interrup��o de recep��o serial definida no arquivo header
//IntRecSerial(void)
#pragma vector = UART1_R_RXNE_vector
__interrupt void IntRecSerial(void)
{
    CAR8 ByteLido;//Armazena o caracter recebido pela serial
    U8 Inicio;//Apontador para localizar o inicio do comando   
    U8 Fim;//Apontador para localizar o fim do comando
    U8 ProcComand;//auxiliar para o escaneamento dos comandos
    U8 EncComand;//caso o comando tenha sido encontrado
    U8 ArgRec;//armazena o status de recep��o de um argumento
    U8 EntreAspas=0;
    //trata
    ByteLido=TermGetByte();//transfere para o ByteLido o valor que se encontra no buffer serial    
//implementa��o
//faz um toupper
    if(ByteLido>=97 && ByteLido<=122)
    {
        ByteLido-=32;
    }
    //trata
    #if ECOAR_CARACTERES == TRUE //verifica se o sistema deve ecoar no terminal
        TermSendByte(ByteLido);//ecoa o byte pela serial
    #endif
    //tratamos a string se 
    if((ByteLido=='\r'//receber um <CR>(enter)
     && ApontBuf>0)//e o buffer j� tenha um caracter armazenado
     || ApontBuf==TamanDoBuf-2)//ou se o buffer estiver cheio (n�o deve ler o ultimo \0)
    {
        Inicio=0;//inicializa o apontador para procurar em toda string
        //encontramos o inicio removendo as informa��es iniciais
        while(BufSerial[Inicio] == ' ' //enquanto for  espa�o
        || BufSerial[Inicio] == '\t'   //ou for \t
        || BufSerial[Inicio] == '\n')  //ou for \n
        {
            Inicio++;//incrementa o apontador
        }        
        //prepara o apontador para localizar o fim do comando
       Fim=Inicio;//inicializa com inicio da posi��o do comando
        //encontramos o fim do comando
        while(BufSerial[Fim] != ' '  //enquanto n�o for espa�o
           && BufSerial[Fim] != '\t' //nem \t 
           && BufSerial[Fim] != '\n' //nem \n
           && BufSerial[Fim] != '='  //nem =
           && BufSerial[Fim] != ':'  //nem : 
           && BufSerial[Fim] != '\0')//nem \0
        {
             Fim++;//incrementa o apontador
        }
        ArgRec=0;//inicializa o flag que informa se o argumento foi recebido
        //verifica se recebeu a coloca��o de um argumento
        if(BufSerial[Fim]=='='//caracter =
        || BufSerial[Fim]==':'//caracter :
        || BufSerial[Fim]==' ')//mensagens com espa�o tamb�m podem receber argumentos
        {
            //seta o flag informando que foi recebido argumento
            ArgRec=1;
        }
        //j� foi localizado o inicio e o fim do comando a ser processado
        //separamos a string de comandos dos parametros fechando a string com o /0
        BufSerial[Fim]='\0';//coloca a termina��o ap�s o comando (sobrepondo o argumento final)
        BufSerial[ApontBuf]='\0';//coloca a termina��o no lugar do enter final do apontador
        EncComand=0;//flag � setado caso o comando seja encontrado
        //efetua a varerdura na lista dos comandos instalados
        for(ProcComand=0;ProcComand<NumComands;ProcComand++)
        {
            //compara o comando da lista de instalados com o comando que foi recebido
            if (CompString(BufSerial+Inicio,ArgumentComand[ProcComand]))
            {
                EncComand=1;//comando foi encontrado                
                break;//e encerra o la�o de repeti��o
            }
        }
        //caso tenha sido localizado o comando
        if(EncComand)
        {
            //recarrega os registradores
            ComandValid=ProcComand;//armazena no registrador o comando encontrado
            ApontBufArs=0;//zera o apontador o buffer de argumentos
            //verifica se foi recebido algum argumento
            if(ArgRec)
            {
                //O in�cio do argumento � o final do comando +1
                Inicio=(U8)(Fim+1);
				//encontramos o inicio do argumento removendo os espa�os iniciais
				while(BufSerial[Inicio] == ' ' //enquanto for  espa�o
				   && BufSerial[Inicio] != '\t' //nem \t
				   && BufSerial[Inicio] != '\n' //nem \n
				   && BufSerial[Inicio] != '\0')//nem \0
				{
					Inicio++;//incrementa o apontador
				}
                //iniciamos o fim do argumento com a mesma posi��o do inicio
                Fim=Inicio;
                //encontramos o fim do comando
//implementa��o
//recebe argumentos com espa�os entre aspas
                while((BufSerial[Fim] != ' ' || EntreAspas)  //enquanto n�o for espa�o
                   && BufSerial[Fim] != '\t' //nem \t 
                   && BufSerial[Fim] != '\n' //nem \n 
                   && BufSerial[Fim] != '\0')//nem \0
                {
                     if(BufSerial[Fim]=='"')
                     {
                        if(EntreAspas)EntreAspas=0;
                        else EntreAspas=1;
                     }
                     Fim++;//incrementa o apontador
                }
                //aqui j� localizamos o fim do argumento
            }//fim do if argumento                
            if(ArgRec)//caso tenha sido recebido algum argumento
            {
                //transfere para o registrador o argumento do comando
                while(Inicio!=Fim)//enquanto o inicio estiver diferente do fim                    
                {
//bug da pilha AT
//tem que tratar o tamanho do buffer de argumentos sen�o pode dar overflow                        
                    BufArgs[ApontBufArs++]=BufSerial[Inicio++];
                    //incrementamos o apontador do buffer e o do inicio do argumento
                }//fim do while
            }
            TerminalTrataComandosInt();//chama a rotina que processa os comandos
        }//fim do if comando encontrado        
        else//comando n�o encontrado
        {
            ApontBufArs=0;//zera o apontador o buffer de argumentos
            while(Inicio!=Fim//enquanto o inicio estiver diferente do fim 
            && ApontBufArs<TamanDoBufArgs)//e n�o tiver enchido o buffer                   
            {
                BufArgs[ApontBufArs++]=BufSerial[Inicio++];
                //incrementamos o apontador do buffer e o do inicio do argumento
            }//fim do while
            //caso n�o consiga copiar todo o buffer ignora a informa��o
            if(Inicio!=Fim)
            {
                ApontBufArs=0;
                BufArgs[0]='\0';
            }
            //o buffer de argumentos n�o possui terminador
            //o terminador � colocado apenas nos comandos recebidos
        }
        //apos tratamento do comando
        ApontBuf=0;//zera o apontador   
    }//fim do if enter ou buffer cheio
    else//caso n�o tenha recebido enter nem o buffer est� cheio
    {
        if(ByteLido=='\n'//caso seja o caracter <LF> (nova linha linha limpa)
        || ByteLido=='\r')//se recebeu um enter significa que n�o recebeu nenhum comando ap�s
        {
            ApontBuf=0;//limpa o buffer de recep��o serial
        }
        else if (ByteLido=='\b')//caso receba um backspace
        {
            if (ApontBuf>0)//caso n�o esteja no inicio do apontador do buffer
            {
                BufSerial[ApontBuf]='\0';//limpa a posi��o do buffer
                ApontBuf--;//decrementa o apontador do buffer
            }
        }
        else//caso n�o seja um backspace
        {
            BufSerial[ApontBuf]=ByteLido;//armazena o byte lido no buffer de recep��o serial
            ApontBuf++;//incrementa a pilha
        }
    }//fim do else de recep��o do enter
}//fim da interrup��o de recep��o serial

//rotinas que n�o recebem paramtros n�o devem chamar �sta rotina para verifica��o simplesmente pois 
//�la vai retorna 0 ao sistema 0 pode ser erro
//rotina retorna a quantidade de argumentos recebidos ap�s o igual(=)
//retorna em decimal 0 se nenhum
//utiliza o apontador do buffer de argumentos para verificar
//cada virgula e comando recebida incrementa um
//se receber virgula (,) no inicio ou no ultimo caracter retorna 0
//se receber duas virgulas na sequencia retorna 0
U8 GetNumArgument(CAR8 *Param,U8 *Apont)
{
    U8 Chars;//auxiliar para armazenar quantos caracteres foram recebidos pelo argumento
    U8 Aux=0;//axiliar que incrementa junto ao apontador
    U8 Arguments=0;//incrementa a cada argumento encontrado
        
    Chars=*Apont;//carrega a quantidade de caracteres recebidos para o apontador
    //para o terminal verifica antes
    //verifica irregualaridades l�gicas na sequencia de parametros recebida
    if(Param[0]!=','//primeiro caracter n�o pode ser v�rgula
    && Param[Chars-1]!=','//�ltimo caracter n�o pode ser v�rgula
    && Chars>0)//foi recebido ao menos 1 caracter de parametro
    {
        Arguments=1;//incrementa em 1 a quantidade de argumentos pois foi recebido um caracter    
        //Enquanto tiver agrumentos para veriicar
        while(Aux!=Chars)
        {
            //cada v�rgula representa um parametro a mais recebido
            if(Param[Aux]==',')
            {
                Arguments++;
                //verifica possiveis erros de continuidade
//ignora pois tem que 
//tratar alguns argumentos sem informa��o entre as v�rgulas
//                if(Param[Aux+1]==',')//ap�s uma virgula n�o pode se receber mais uma
//                {
//                     Arguments=0;//zera os argumentos recebidos
//                     break;//encerra o la�o wile
//                }
            }
            //aqui podem ser tratados mais erros de continuidade
            
            Aux++;//incrementa um para a verifica��o do pr�ximo caracter
        }
    }
    return Arguments;//retorna a quantidade de argumentos recebidos
}//fim da rotina que retorna o n�mero de argumentos

//converte um argumento recebida pelo terminal para uma U16 (16 bits)
//retorna 1 se conseguiu
//recebe a string pelo str
//coloca o valor no apontador de valor
//verifica caracteres fora da escala decimal e retorna como erro
//no argumento se informa qual argumento da string que se deseja converter
//no apontador recebe quantos caracteres foram recebidos
//no argum o primeiro argumento � o de n�mero 0
U8 ArgumentToInt(U8 Argum,U16 *Valor,CAR8 *Param,U8 *Apont)
{
    U8 status=0;//deve ser setado caso ocorra com sucesso a convers�o
    U8 ArgsValidos;//armazena a quantidade de argumentos reais
    U8 Aux=0;//axiliar que incrementa junto ao apontador
    U8 Arguments=0;//incrementa a cada argumento encontrado
    U8 BufTemp[5];//caracteres que podem conter em uma int
    U8 ApontTemp=0;//apontador para a buffer temr�ria
    U32 ValorTemp=0;//armazena o valor tempor�riamente
    U16 AuxMult=1;//auxiliar de multiplica��o
    
    //verifica se o argumento existe   
    //chama a rotina que devolve a quantidade de argumentos recebidos
    ArgsValidos=GetNumArgument(Param,Apont);//checa erros de dados     
    //caso o argumento esteja na lista de argumentos
    if(Argum<=ArgsValidos)
    {
        //conta quantos caracteres foram recebidos
        //Enquanto tiver agrumentos para veriicar
        while(Aux!=*Apont)//apontador � a ultima posi��o de argumento recebido
        {
            //cada v�rgula representa um parametro a mais recebido
            if(Param[Aux]==',')
            {
                Arguments++;

            }//se n�o tivermos recebido uma virgula
            else if(Argum==Arguments) //caso esteja jamos no argumento desejado
            {
                //verifica se o caracter n�o contem um valor ilegal
                if(Param[Aux]<='9' //menor que 9
                && Param[Aux]>='0') //e maior que 0
                {
                    //caso o buffer esteja cheio
                    if(ApontTemp>=5)
                    {
                        ApontTemp=0;//zera a recep��o de caracter v�lido
                        break;//encerra o la�o de repeti��o                        
                    }
                    //armazena no buffer o caracter recebido
                    BufTemp[ApontTemp]=(U8)(Param[Aux]-'0');//decrementa o caracter (0) mantendo somente o valor real
                    ApontTemp++;//incrementa o apontador
                }
                else//comando ilegal
                {
                    ApontTemp=0;//zera a recep��o de caracter v�lido
                    break;//encerra o la�o de repeti��o
                }
            }
            Aux++;//incrementa um para a verifica��o do pr�ximo caracter
        }//fim do while
        //caso tenha recebido algum caracter v�lido
        if(ApontTemp>0)
        {
            // ent�o juntamos os bytes do buffer para formar o valor a ser retornado
            do
            {
                ValorTemp+=BufTemp[ApontTemp-1]*AuxMult;//pegamos o ultimo byte pois � o menos significativo
                AuxMult*=10;//multiplica por 10 para armazenar o pr�ximo registrador
                ApontTemp--;//decrementa um do apontador    
            }while(ApontTemp);//enquanto tiver buffer retoma a rotina                

            //verificamos se o valor armazenado n�o � maior que uma int
            if(ValorTemp<=65535)
            {
                *Valor=(U16)ValorTemp;//colocamos no valor solicitado o valor que foi calculado
                status=1;//setamos o flag de sucesso
            }
        }//fim do if apont maior que zero
    }//fim do if argumento na lista de agumentos
    return status;//retorna o status da verifica��o
}//fim do string to int
/*
Rotina transfere um argumento recebido para a string

Espa��s n�o s�o aceitos
Solicita:
Qual argumento deve ser transferido 0,1,2,3,etc
Endere�o de uma matriz para armazenar a string
Endere�o de um byte para armazenar a quantidade de caracteres recebidos
Tamanho da matriz onde pode ser armazenado o argumento
Endere�o dos parametros
Endere�o do apontador

Retorna:
a quantidade de caracteres
os caracteres na matriz apontada
sucesso 1 erro 0
*/
U8 ArgumentToString(U8 Argum,
CAR8 *Matriz,
U8 *QunatRec,
U8 Quanto,
CAR8 *Param,U8 *Apont)
{
    U8 status=0;//deve ser setado caso consiga transferir
    U8 ArgsValidos;//armazena a quantidade de argumentos reais
    U8 Aux=0;//axiliar que incrementa junto ao apontador    
    U8 Arguments=0;//incrementa a cada argumento encontrado
    U8 ApontTemp=0;//apontador para a buffer temr�ria
    
    //verifica se o argumento existe   
    //chama a rotina que devolve a quantidade de argumentos recebidos
	ArgsValidos=GetNumArgument(Param,Apont);//checa erros de dados
    //caso o argumento esteja na lista de argumentos
    if(Argum<=ArgsValidos)
    {
        //conta quantos caracteres foram recebidos
        //Enquanto tiver agrumentos para veriicar
        while(Aux!=*Apont)//apontador � a ultima posi��o de argumento recebido
        {
            //cada v�rgula representa um parametro a mais recebido
            if(Param[Aux]==',')
            {
                Arguments++;//vai para o pr�ximo argumento

            }//se n�o tivermos recebido uma virgula
            else if(Argum==Arguments) //caso estejajamos no argumento desejado
            {
                //transfere o caracter exatamente como recebido
                //Pelo Quanto deve ser defnido o limite de bytes do buffer
                //caso o buffer esteja cheio
                //o -1 significa o terminador de string que ainda deve ser inserido \0
                if(ApontTemp>=Quanto-1)
				{
                    ApontTemp=0;//zera a recep��o de caracter v�lido
                    break;//encerra o la�o de repeti��o                        
                }
                //armazena no buffer o caracter recebido
                Matriz[ApontTemp]=Param[Aux];//transfere igual
                ApontTemp++;//incrementa o apontador
            }//fim do else if arg=argumentos
            Aux++;//incrementa um para a verifica��o do pr�ximo caracter
        }//fim do while
        //caso tenha recebido algum caracter v�lido
        if(ApontTemp>0)
        {
                status=1;//setamos o flag de sucesso
                *QunatRec=ApontTemp;//colocamos no apontador o numero de caracteres recebidos
                Matriz[ApontTemp]='\0';//coloca o terminador no argumento
        }//fim do if apont maior que zero
    }//fim do if argumento na lista de agumentos
    return status;//retorna o status da verifica��o
}//fim do argumento to string

//efetua a compara��o de um argumento indicado com uma string passada
//retorna 1 se confere
U8 ComparaArgumentString(CAR8 *Param,U8 *Apont,U8 Argumento,CAR8 *Comando)
{
	U8 MatrAux[TamMaxArg];//matriz auxiliar que receber� o argumento
	U8 QuantBytes;//armazerna quantos caracteres foram recebidos na matriz

    if(ArgumentToString(Argumento,(CAR8*)MatrAux,&QuantBytes,TamMaxArg,Param,Apont))//Efetua a c�pia do argumento espec�fico para uma matriz auxiliar
    {
		if(CompString((CAR8*)MatrAux,Comando))//Compara o argumento da matriz auxiliar com o arqgumento passado
        {
            return 1;
        }
    }
    return 0;
}
//---------------------------------------------------------------------------------------------------------
//fim do arquivo
