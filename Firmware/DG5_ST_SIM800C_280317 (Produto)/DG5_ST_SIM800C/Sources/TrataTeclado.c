//TrataTeclado.c

#include "TrataTeclado.h"
#include "derivative.h"         /* include peripheral declarations                  */
#include "MenuManager.h"
#include "TrataBuzzer.h"

//Inicializa o teclado
void InitTeclado(void)
{
    ADC_CSR=(MASK_ADC_CSR_CH&0x04);//converte do canal 0 ao 4
    ADC_CR1=(MASK_ADC_CR1_SPSEL&0x70)|MASK_ADC_CR1_ADON;//prescaler = 18
    ADC_CR2=MASK_ADC_CR2_SCAN|MASK_ADC_CR2_ALIGN;//Right alignment
    ADC_CR3=0;    
}

//Efetua a leitura do teclado e retorna a tecla que foi pressionada
U8 LeTeclado(void)
{
    U8 Digito=DigNennhuma;//inicializa sem nenhuma tecla pressionada
    U8 i;
    U8 Nib=0;//evita duas teclas pressionadas ao mesmo tempo
    U16 TempAD;
    U16 ADColuna1;//AD4
    U16 ADColuna2;//AD3
    U16 ADColuna3;//AD2

    //zera os registradores
    ADColuna1=0;
    ADColuna2=0;
    ADColuna3=0;
    //verifica a coluna 5 vezes
    for(i=0;i<AdSamples;i++)
    {
        //Single scan mode
        ADC_CSR_EOC=0;//clear end of conversion bit
        ADC_CR1_ADON=1;//start single scan
        while(!ADC_CSR_EOC);//whait the last channel has been converted
        //Coluna 1
        TempAD=ADC_DB4RH;
        TempAD<<=8;
        TempAD|=ADC_DB4RL;
        ADColuna1+=TempAD;
        //Coluna 2
        TempAD=ADC_DB3RH;
        TempAD<<=8;
        TempAD|=ADC_DB3RL;
        ADColuna2+=TempAD;
        //Coluna 3
        TempAD=ADC_DB2RH;
        TempAD<<=8;
        TempAD|=ADC_DB2RL;
        ADColuna3+=TempAD;
    }    
    ADColuna1/=AdSamples;
    ADColuna2/=AdSamples;
    ADColuna3/=AdSamples;
          if(ADColuna1<AdLin1Sup && ADColuna1>AdLin1Inf){
        Digito=DigUm;
        Nib++;
    }else if(ADColuna1<AdLin2Sup && ADColuna1>AdLin2Inf){
        Digito=DigQuatro;
        Nib++;
    }else if(ADColuna1<AdLin3Sup && ADColuna1>AdLin3Inf){
        Digito=DigSete;
        Nib++;
    }else if(ADColuna1<AdLin4Sup && ADColuna1>AdLin4Inf){
        Digito=DigProg;
        Nib++;
    }
          if(ADColuna2<AdLin1Sup && ADColuna2>AdLin1Inf){
        Digito=DigDois;
        Nib++;
    }else if(ADColuna2<AdLin2Sup && ADColuna2>AdLin2Inf){
        Digito=DigCinco;
        Nib++;
    }else if(ADColuna2<AdLin3Sup && ADColuna2>AdLin3Inf){
        Digito=DigOito;
        Nib++;
    }else if(ADColuna2<AdLin4Sup && ADColuna2>AdLin4Inf){
        Digito=DigZero;
        Nib++;
    }
          if(ADColuna3<AdLin1Sup && ADColuna3>AdLin1Inf){
        Digito=DigTres;
        Nib++;
    }else if(ADColuna3<AdLin2Sup && ADColuna3>AdLin2Inf){
        Digito=DigSeis;
        Nib++;
    }else if(ADColuna3<AdLin3Sup && ADColuna3>AdLin3Inf){
        Digito=DigNove;
        Nib++;
    }else if(ADColuna3<AdLin4Sup && ADColuna3>AdLin4Inf){
        Digito=DigEnter;
        Nib++;
    }

    //trata ru�do e duplica��o de tecla tornado Nib diferente de 1 erro
    if(Nib!=1)Digito=DigNennhuma;
    return(Digito);//retorna valor
}//fim do teclado

//evita o tratamento da mesma tecla 2 vezes
U8 Antiloop=DigNennhuma;

U8 ContIgnora=0;
U8 ContPress=0;

//permanece monitorando o teclado a cada 10ms 
//quando for pressionada uma tecla chama a rotina de tratamento
void TarefaTrataTeclado(void)
{
    U8 TeclaLida;
    
    if(ContIgnora)
    {
        ContIgnora--;
        return;
    }
    TeclaLida=LeTeclado();
    if(TeclaLida!=DigNennhuma)
    {
        if(ContPress<TempoPress)
        {
            ContPress++;
            return;
        }
    }else{
        ContPress=0;
    }
    //Trata a tecla
    if(TeclaLida!=Antiloop)//verifica se a leitura � diferente da mem�ria
    {
        Antiloop=TeclaLida;//carrega a mem com a leitura
        if(TeclaLida!=DigNennhuma)//se foi pressionado alguma tecla 
        {
            //gera um beep no teclado
            TipoBeep=0;//curto
            ContBeep=1;//1 beep
            //chama a rotina que trata o teclado
            MenuTrataDigito(TeclaLida);
            ContIgnora=TempoIgnora;//delay para aceitar a pr�xima tecla
        }
    }
}

//fim do arquivo
