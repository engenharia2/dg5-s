#ifndef TICK_TIMER_H
#define TICK_TIMER_H

#include "Tipos_v2.h"

struct TickStruct
{
	U16 Start;		//diferenša de tempo
	U16 Intervalo;	//intervalo de tempo
};

typedef struct TickStruct TickS;

void TickClockInit(void);
void TickSet(U16 Intervalo,TickS * T);
void TickChange(U16 Intervalo,TickS * T);
void TickReset(TickS * T);
U8 TickExpired(TickS * T);
void TickRestart(TickS * T);
//void TickApp(void);

#endif
//fim do arquivo
