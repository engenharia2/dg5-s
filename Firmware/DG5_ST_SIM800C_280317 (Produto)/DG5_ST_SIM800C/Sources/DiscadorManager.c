//DiscadorManager.c

#include "DiscadorManager.h"
#include "EmulaEEprom.h"

//----------------------------------------------------------------------
void DiscProgramTelef(U8 Pos,U8 * Buf,U8 Compr)
{    
    U8 z=0;
    
    if(Compr>0//recebido telefone
    && Compr<=DiscMaxDigit//comprimento dentro do limite
    && Pos<DiscNumMem)//caso esteja dentro do range
    {
        while(z<(Compr/2))
        {
            WriteEEprom(((DiscMaxDigit/2)*Pos)+z+AddrDisc,Buf[z]);
            z++;
        }
        if(Compr%2)//se for impar grava o ultimo d�gito e encerra o telefone
        {
            WriteEEprom(((DiscMaxDigit/2)*Pos)+z+AddrDisc,(Buf[z]|0x0F));
        }
        else//se for par verifica se encheou a mem�ria ou tem que finalizar
        {
            if(Compr<DiscMaxDigit)
            {
                WriteEEprom(((DiscMaxDigit/2)*Pos)+z+AddrDisc,0xFF);
            }
        }
    }
}
//----------------------------------------------------------------------
U8 DiscGetDigito(U8 Pos,U8 Digit)
{
    U8 Digito=DigitNull;
    
    if(Pos<DiscNumMem//caso esteja dentro do range
    && Digit<DiscMaxDigit)
    {
        Digito=ReadEEprom(((DiscMaxDigit/2)*Pos)+(Digit/2)+AddrDisc);
        if(!(Digit%2))//se o d�gito for par desloca para a direita
        {
            Digito>>=4;
        }
        Digito&=0x0F;//limpa o d�gito
    }
    return(Digito);
}
//----------------------------------------------------------------------
void DiscApagaTelef(U8 Pos)
{
    if(Pos<DiscNumMem)//caso esteja dentro do range
    {
        WriteEEprom(((DiscMaxDigit/2)*Pos)+AddrDisc,0xFF);
    }
}
//----------------------------------------------------------------------
void DiscApagaAllTelef(void)
{
    U8 Pos;
        
    for(Pos=0;Pos<DiscNumMem;Pos++)
    {
        WriteEEprom(((DiscMaxDigit/2)*Pos)+AddrDisc,0xFF);
    }
}
//----------------------------------------------------------------------
//fim do arquivo

