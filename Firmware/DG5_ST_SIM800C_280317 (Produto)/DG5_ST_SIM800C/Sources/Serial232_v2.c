//Serial232_v2.c
//Lib para comunica��o serial
//Vers�o 2
//Joel Langer

#include "Serial232_v2.h"//inteface e rotinas da comunica��o serial
#include "derivative.h" //include peripheral declarations

//retorna o byte que foi recebido pela serial
//deve ser utilizado na interrup��o de recep��o
CAR8 SCIGetByte(void)
{
    U8 ByteLido;
    //apagamos o flag de interrup��o serial lendo o registrador
    ByteLido=UART1_DR;//efetua a leitura do byte do buffer serial
    return((CAR8)ByteLido);//retorna o valor que foi carregado
}

CAR8 BuffTx[TamanBufTx];//buffer de transmiss�o
U8 TxBufRead;//apontador de leitura no buffer
U8 TxBufWrite;//apontador de escrita no buffer
U8 TxBufStk;

void InitTxSerial(void)
{
    TxBufRead=0;
    TxBufWrite=0;
    TxBufStk=0;
    
    //9600,8,N,1
    UART1_BRR1=0x68;//16MHz/9600=0x6802
    UART1_BRR2=0x02;
    UART1_CR1=0;
    UART1_CR2=MASK_UART1_CR2_REN|MASK_UART1_CR2_TEN|MASK_UART1_CR2_RIEN;
    UART1_CR3=0;
    UART1_CR4=0;
    UART1_CR5=0;
    UART1_GTR=0;
    UART1_PSCR=0;
}

void BufPutNextByte(void)
{
    UART1_DR=(U8)BuffTx[TxBufRead];//coloca o byte no buffer serial
    TxBufRead++;//incrementa a posi��o de leitura
    TxBufStk--;//decrementa a pilha
    if(TxBufRead>=TamanBufTx)
    {
        TxBufRead=0;
    }
}

//interup��o de transmiss�o completa
//Transmit Data Register empty interrupt
//IntTxSerial(void)
#pragma vector = UART1_T_TXE_vector
__interrupt void IntTxSerial(void)
{
    if(TxBufRead==TxBufWrite)//caso n�o possuam mais bytes para serem transmitidos
    {
        UART1_CR2_TIEN=0;//desabilita a interrup��o de transmiss�o completa
    }
    else//ainda possuem bytes para serem transmitidos
    {
        BufPutNextByte();
    }
}

//envia um byte ao terminal serial
void SCISendByte(CAR8 Valor)
{
    BuffTx[TxBufWrite]=Valor;//coloca o byte no buffer
    TxBufWrite++;//incrementa a posi��o de escrita
    TxBufStk++;//incrementa a pilha
    if(TxBufWrite>=TamanBufTx)
    {
        TxBufWrite=0;
    }
    if(UART1_CR2_TIEN==0)//se a interup��o de transmiss�o completa n�o est� habilitada
    {
        //transmite o primeiro byte
        BufPutNextByte();
        UART1_CR2_TIEN=1;//habilita a interrup��o
    }
}

//envia uma string ao terminal
void SCISendString(CAR8 * c)
{
    while(*c)
    {
        SCISendByte(*c++);
    }
}

//envia 8 bits em bin�rio ao terminal
void SCISendBin(U8 Valor)
{
    U8 Aux=8;
    while(Aux)
    {
        if(Valor&128)
        {
            SCISendByte('1');
        }else{
            SCISendByte('0');
        }
        Valor<<=1;
        Aux--;
    }
}

//envia um valor em hexadecimal ao terminal
const CAR8 TabelaAux[17]={"0123456789ABCDEF"};
void SCISendHex(U8 Valor)
{
    SCISendByte(TabelaAux[(Valor>>4)&15]);    
    SCISendByte(TabelaAux[(Valor)&15]);
}

//escreve uma int 0 a 65535 no terminal
void SCISendInt(U16 Valor)
{
    CAR8 CharTemp[5];//armazena os 5 caracteres tempor�riamente
    U8 Aux=5;//auxiliar
    U16 DivTemp=10000;//auxiliar para diviz�o
    U8 Mostra=0;//mostra o valor
    while(Aux)//enquanto for
    {
        CharTemp[Aux-1]=(CAR8)(Valor/DivTemp);//cria o caracter
        Valor-=CharTemp[Aux-1]*DivTemp;//decrementa o valor
        DivTemp/=10;//decrementa o divisor
        Aux--;//decrementa o auxiliar
        if(CharTemp[Aux]//mostra se o valor for maior que 0
        ||Mostra//ou j� tenha encontrado um d�gito maior que 0
        ||!Aux)//ou for o �ltimo d�gito mesmo sendo 0
        {
            Mostra=1;//informa que todos os posteriores devem ser mostrados
            SCISendByte((CAR8)(CharTemp[Aux]+'0'));    
        }
    }
}

//escreve em decimal valor at� 32 bits 0 a 4294967295 no terminal
void SCISendLong(U32 Valor)
{
    CAR8 CharTemp[10];//armazena os 10 caracteres tempor�riamente
    U8 Aux=10;//auxiliar
    U32 DivTemp=1000000000;//auxiliar para diviz�o
    U8 Mostra=0;//mostra o valor
    while(Aux)//enquanto for
    {
        CharTemp[Aux-1]=(CAR8)(Valor/DivTemp);//cria o caracter
        Valor-=CharTemp[Aux-1]*DivTemp;//decrementa o valor
        DivTemp/=10;//decrementa o divisor
        Aux--;//decrementa o auxiliar
        if(CharTemp[Aux]//mostra se o valor for maior que 0
        ||Mostra//ou j� tenha encontrado um d�gito maior que 0
        ||!Aux)//ou for o �ltimo d�gito mesmo sendo 0
        {
            Mostra=1;//informa que todos os posteriores devem ser mostrados
            SCISendByte((CAR8)(CharTemp[Aux]+'0'));    
        }
    }
}
//fim do arquivo
