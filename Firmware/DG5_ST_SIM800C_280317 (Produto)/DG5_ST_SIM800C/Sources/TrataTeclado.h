#ifndef TRATA_TECLADO_H
#define TRATA_TECLADO_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

#define AdLin1Inf   264
#define AdLin2Inf   338
#define AdLin3Inf   471
#define AdLin4Inf   648
#define AdLin1Sup   310
#define AdLin2Sup   397
#define AdLin3Sup   553
#define AdLin4Sup   760

#define AdSamples   10

//Define os valores que s�o retornados
#define DigNennhuma 254
//teclado num�rico
#define DigUm       1
#define DigDois     2
#define DigTres     3
#define DigQuatro   4
#define DigCinco    5
#define DigSeis     6
#define DigSete     7
#define DigOito     8
#define DigNove     9
#define DigZero     0
//teclado operacional
#define DigProg     10
#define DigEnter    11

#define TempoPress  5//60ms para aceitar tecla pressionada
#define TempoIgnora 14//150ms para aceitar a pr�xima tecla 

void InitTeclado(void);
/*
    //a cada 10ms no loop principal
    (void)InstalaRotina(&TarefaTrataTeclado,10,MODO_LOOP_TEMPO);
*/
void TarefaTrataTeclado(void);

#endif
//fim do arquivo
