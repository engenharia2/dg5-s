#include "TrataPWM.h"
#include "derivative.h"         /* include peripheral declarations                  */

void InitPWM(void)
{
    //TIM1 CH3
    //16MHz/128 = 125 kHz/125 = 1kHz(1ms)
    
    TIM1_CR1=0;//Counter disable
    //TIM1_CR2
    //TIM1_SMCR
    //TIM1_ETR
    //TIM1_IER
    //TIM1_SR1
    //TIM1_SR2
    TIM1_EGR=0;
    //TIM1_CCMR1
    //TIM1_CCMR2
    TIM1_CCMR3=0;
    TIM1_CCMR3_OC3M=6;//PWM mode 1
    //TIM1_CCMR4
    //TIM1_CCER1
    TIM1_CCER2=0;//Capture/compare 3 output disable
    TIM1_CNTRH=0;
    TIM1_CNTRL=0;
    TIM1_PSCRH=0;
    TIM1_PSCRL=128;//Prescaler
    TIM1_ARRH=0;
    TIM1_ARRL=125;//Auto-reload
    //TIM1_RCR
    //TIM1_CCR1H
    //TIM1_CCR1L
    //TIM1_CCR2H
    //TIM1_CCR2L
    TIM1_CCR3H=0;
    TIM1_CCR3L=62;//Capture/compare
    //TIM1_CCR4H
    //TIM1_CCR4L
    TIM1_BKR=MASK_TIM1_BKR_MOE;//outputs are enabled
    //TIM1_DTR
    //TIM1_OISR
    PD_DDR_DDR4=1;//configura o pino como sa�da em 0
    PD_CR1_C14=1;
    PD_ODR_ODR4=0;
}

void StartPWM(U16 FreqHz)
{
    U16 AutoReload;
    U16 CaptureCompare;
    
    //125000 (125kHz)
    AutoReload=125000/FreqHz;
    
    CaptureCompare=AutoReload/2;
    TIM1_CR1=0;
    TIM1_ARRL=AutoReload&0xFF;//Auto-reload
    AutoReload>>=8;
    TIM1_ARRH=AutoReload&0xFF;;
    TIM1_CCR3L=CaptureCompare&0xFF;//Capture/compare
    CaptureCompare>>=8;
    TIM1_CCR3H=CaptureCompare&0xFF;
    TIM1_CCER2=MASK_TIM1_CCER2_CC3E;//Capture/compare 3 output enable
    TIM1_CNTRH=0;
    TIM1_CNTRL=0;
    TIM1_CR1=MASK_TIM1_CR1_CEN;//Counter enabled
}

void StopPWM(void)
{
    TIM1_CR1=0;
    TIM1_CCER2=0;
    PD_DDR_DDR4=1;//configura o pino como sa�da em 0
    PD_CR1_C14=1;
    PD_ODR_ODR4=0;
}
//fim do arquivo
