//TrataPanico.c

#include "TrataPanico.h"
#include "derivative.h"         /* include peripheral declarations                  */

#define TempoPanicoRest    20//200ms para restaurar o status
#define TempoPanicoAtiv    100//1000ms para definir como acionado

struct
{
    U8 Stat;
    U16 Timer;
}SensorPanico;

//retorna 1 se estiver acionado 0 se n�o
U8 StatusPanico(void)
{
    return(SensorPanico.Stat);
}

void InitPanico(void)
{
    DIR_ENT_PANICO=0;
    CR1_ENT_PANICO=0;

    SensorPanico.Stat=0;
    SensorPanico.Timer=0;
}

//instalada para ser executada a cada 10ms no loop principal
//trata o tempo para validar o acionamento da Panico
void TarefaTrataPanico(void)
{
    if(ENT_PANICO)//acionado
    {
        if(!SensorPanico.Stat)//flag ainda n�o foi setado
        {
            SensorPanico.Timer++;//incrementa o timer
            if(SensorPanico.Timer>=TempoPanicoAtiv)
            {
                SensorPanico.Timer=0;//encerra o timer
                SensorPanico.Stat=1;//seta o flag
            }
        }
        else//flag j� foi setado
        {
            SensorPanico.Timer=0;//apaga o timer    
        }
    }
    else//n�o acionado
    {
        if(SensorPanico.Stat)//flag ainda n�o foi apagado
        {
            SensorPanico.Timer++;//incrementa o timer
            if(SensorPanico.Timer>=TempoPanicoRest)
            {
                SensorPanico.Timer=0;//encerra o timer
                SensorPanico.Stat=0;//apaga o flag
            }
        }
        else//flag j� foi apagado
        {
            SensorPanico.Timer=0;//apaga o timer    
        }
    }
}
//fim do arquivo
