//LedManager_v1.c

#include "LedManager_v1.h"
#include "DriverLedsMtpx.h"

TipoLeds LedsStat[NumLedsSist];//status dos leds

void InitLeds(void)
{
    U8 i;

    for(i=0;i<NumLedsSist;i++)
    {
        LedsStat[i].Bits.Modo=LedOff;    
    }
}

U8 LedsLTimer=0;
U8 LedsLTimState=0;
U8 LedsSTimer=0;
U8 LedsSTimState=0;

//instalada a cada 100ms no m�do interrup��o de tempo
void ThreadPiscaLeds(void)
{
    U8 i,x;

    if(LedsLTimer)LedsLTimer--;//decrementa
    if(LedsSTimer)LedsSTimer--;//decrementa
    if(!LedsLTimer)//Long Timer
    {
        LedsLTimer=12;//1,2 segundo ligado e desligado
        if(LedsLTimState)
        {
            LedsLTimState=0;
        }
        else
        {
            LedsLTimState=1;
        }
    }
    if(!LedsSTimer)//Short Timer
    {
        if(LedsSTimState)
        {            
            LedsSTimer=8;//0,8 segundo desligado
            LedsSTimState=0;
        }
        else
        {
            LedsSTimer=1;//0,1 segundo ligado
            LedsSTimState=1;
        }
    }
    
    for(i=0;i<NumLedsSist;i++)
    {
        x=LedsStat[i].Bits.Modo;
        switch(x)
        {
            case LedOff:
                LedsStat[i].Bits.Estado=0;//desliga
            break;
            
            case LedOn:
                LedsStat[i].Bits.Estado=1;//liga
            break;
            
            case LedAlterna:
                LedsStat[i].Bits.Estado=LedsLTimState;
            break;
            
            case LedFlash:
                LedsStat[i].Bits.Estado=LedsSTimState;
            break;
        }
    }
    
    //envia o estado para os leds
    Leds.sBits.led3=LedsStat[3].Bits.Estado;
    Leds.sBits.led2=LedsStat[2].Bits.Estado;
    Leds.sBits.led1=LedsStat[1].Bits.Estado;
    Leds.sBits.led0=LedsStat[0].Bits.Estado;
}
//fim do arquivo
