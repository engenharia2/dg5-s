//TrataDsp.c

#include "TrataDsp.h"
#include "derivative.h"         /* include peripheral declarations                  */

#define TempoDspRest    20//200ms para restaurar o status
#define TempoDspAtiv    300//3000ms para definir como acionado

struct
{
    U8 Stat;
    U16 Timer;
}SensorDsp;

//retorna 1 se estiver acionado 0 se n�o
U8 StatusDsp(void)
{
    return(SensorDsp.Stat);
}

void InitDsp(void)
{
    DIR_ENT_DSP=0;
    CR1_ENT_DSP=0;

    SensorDsp.Stat=0;
    SensorDsp.Timer=0;
}

//instalada para ser executada a cada 10ms no loop principal
//trata o tempo para validar o acionamento da Dsp
void TarefaTrataDsp(void)
{
    if(ENT_DSP)//acionado
    {
        if(!SensorDsp.Stat)//flag ainda n�o foi setado
        {
            SensorDsp.Timer++;//incrementa o timer
            if(SensorDsp.Timer>=TempoDspAtiv)
            {
                SensorDsp.Timer=0;//encerra o timer
                SensorDsp.Stat=1;//seta o flag
            }
        }
        else//flag j� foi setado
        {
            SensorDsp.Timer=0;//apaga o timer    
        }
    }
    else//n�o acionado
    {
        if(SensorDsp.Stat)//flag ainda n�o foi apagado
        {
            SensorDsp.Timer++;//incrementa o timer
            if(SensorDsp.Timer>=TempoDspRest)
            {
                SensorDsp.Timer=0;//encerra o timer
                SensorDsp.Stat=0;//apaga o flag
            }
        }
        else//flag j� foi apagado
        {
            SensorDsp.Timer=0;//apaga o timer    
        }
    }
}
//fim do arquivo
