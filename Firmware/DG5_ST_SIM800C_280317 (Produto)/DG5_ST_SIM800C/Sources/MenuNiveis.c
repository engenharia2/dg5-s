//MenuNiveis.c

#include "MenuNiveis.h"
#include "MenuManager.h"
#include "TrataTeclado.h"
#include "DiscadorManager.h"
#include "TrataPgm.h"
#include "GerenciadorGprs_v1.h" /* Ger�nciamento das tarefas do sistema Gprs        */

//----------------------------------------------------------------------
//apaga todas as configura��es
void SetMasterReset(void)
{
    DiscApagaAllTelef();
    SetModoSms(SmsModoOn);
    SetTempoPgm(1);
    SetModoPgm(PgmModoPls);
    SetModoMoni(MoniModoOff);
}
void MPreMasterReset(void)
{
    TeC.Beep=0;//cancela a gera��o dos 2 beeps ao entrar no n�vel
}
//apaga todas configura��es confirma com y
void MMasterReset(void)
{
    if(TeC.Digit==DigProg)//n�o cancelamento
    {
         GoMeSelNiv();//n�o gera beep de erro
    }
    else if(TeC.Digit==DigEnter)//confirma o comando
    {
        SetMasterReset();
        TeC.Beep=Beep2Curtos;//beeps curtos informando sucesso
        GoMeRaiz();
    }
    else
    {
        TeC.Beep=Beep1Longo;
        GoMeRaiz();
    }    
}
//----------------------------------------------------------------------
//fixo menu raiz do sistema
void MPrincipal(void)
{
    
	if(TeC.Digit==DigProg)//acesos ao menu de programa��o
	{
        GoMeSelNiv();
	}
	else
	{
	    TeC.Beep=Beep1Longo;
	}
	TeC.Apont=0;//limpa o buffer de d�gitos
}

//----------------------------------------------------------------------
U8 PosDisc=0;//armazena a posi��o para programa��o do discador
//----------------------------------------------------------------------
//cadastra-apaga telefone 1
void MPreProgTelefPos1(void)
{
    PosDisc=0;
}
//----------------------------------------------------------------------
//cadastra-apaga telefone 2
void MPreProgTelefPos2(void)
{
    PosDisc=1;
}
//----------------------------------------------------------------------
//cadastra-apaga telefone 3
void MPreProgTelefPos3(void)
{
    PosDisc=2;
}
//----------------------------------------------------------------------
//cadastra-apaga telefone 4
void MPreProgTelefPos4(void)
{
    PosDisc=3;
}
//----------------------------------------------------------------------
//cadastra-apaga telefone 5
void MPreProgTelefPos5(void)
{
    PosDisc=4;
}
//----------------------------------------------------------------------
//cadastra-apaga telefone posi��o selecionada
//(confirma com y) (tem pausa de 2 segundos e fun��o *)(clicando y sem nenhum d�gito limpa a mem)
void MProgTelef(void)
{
	//se receber N cancela a altera��o
	if(TeC.Digit==DigProg)//tecla de cancelamento
	{
        GoMeSelNiv();
	}
	else if(TeC.Digit<10 || TeC.Digit==DigEnter)
	{
	    
    	if(TeC.Apont>=DiscMaxDigit//limite de d�gitos da mem�ria de programa��o de um telefone
    	|| TeC.Apont>=LimBufTec//limita pelo tamanho do buffer
    	|| TeC.Digit==DigEnter)//yes salvar
    	{
    		if(TeC.Apont)//caso possua um d�gito no buffer programa
    		{
    		    DiscProgramTelef(PosDisc,(U8*)TeC.Buf,TeC.Apont);
    		}
    		else//se n�o possuir nenhum d�gito apaga
    		{
    		    DiscApagaTelef(PosDisc);
    		}
    		TeC.Beep=Beep2Curtos;//beeps curtos informando sucesso
            GoMeRaiz();
    	}
	}
	else
	{
		TeC.Beep=Beep1Longo;//beep de erro
	}
}
//----------------------------------------------------------------------
void MPreTestTelef(void)
{
    TestDisc=1;
    GoMeRaiz();
}
//----------------------------------------------------------------------
//apaga todos os telefones
void MPreApagaAllTel(void)
{
    DiscApagaAllTelef();
    GoMeRaiz();
}
//----------------------------------------------------------------------
//cadastra tempo de pgm em segundos 3 d�gitos ou Y para confirmar
//Y sem nenhum d�gito programa o tempo padr�o
void MSetPgmTPls(void)
{
    U16 Tempo;//tempo de programa��o
	
	//se receber N cancela a altera��o
	if(TeC.Digit==DigProg)//tecla de cancelamento
	{
        GoMeSelNiv();
	}
	else if(TeC.Digit==DigEnter || TeC.Apont>=3)//caso tenha sido pressionada a tecla Y ou 3 d�gitos
	{
	    if(TeC.Apont)//caso algum d�gito tenha sido pressionado
	    {
            Tempo=TeC.Buf[0].Nibble.hsb;//armazena o primeiro d�gito
            if(TeC.Apont>=2)//caso dois ou mais d�gitos tenham sido pressionados
            {
                Tempo*=10;//multiplica por 10
                Tempo+=TeC.Buf[0].Nibble.lsb;//soma o segundo d�gito
            }
            if(TeC.Apont>=3)//caso tr�s ou mais d�gitos tenham sido pressionados
            {
                Tempo*=10;//multiplica por 10
                Tempo+=TeC.Buf[1].Nibble.hsb;//soma o terceiro d�gito
            }
            if(Tempo>999)Tempo=999;//m�ximo 999 n�o tem como digitar mais que 999
            if(Tempo<1)Tempo=1;//m�nimo 1
	        SetTempoPgm(Tempo);//em segundos
	    }
	    else//caso nenhum d�gito tenha sido pressionado programa o tempo padr�o
	    {
	        SetTempoPgm(1);//segundos
	    }	
		TeC.Beep=Beep2Curtos;//beeps curtos informando sucesso
        GoMeRaiz();
	}
	else if(TeC.Digit<10)//caso tenha recebido um d�gito
	{//n�o gera beep
	}
	else
	{
		TeC.Beep=Beep1Longo;//beep de erro
	}
}
//----------------------------------------------------------------------
//habilita a pgm como reten��o
void MPreSetPgmRet(void)
{
    SetModoPgm(PgmModoRet);//reten��o
    GoMeRaiz();
}
//----------------------------------------------------------------------
//habilita a pgm como pulso
void MPreSetPgmPls(void)
{
    SetModoPgm(PgmModoPls);//pulso
    GoMeRaiz();
}
//----------------------------------------------------------------------
//comanda a pgm
void MPreTestPgm(void)
{
    AcionaPgm();
    GoMeRaiz();
}
//----------------------------------------------------------------------
//ativa sms
void MPreSetSmsOn(void)
{
    SetModoSms(SmsModoOn);
    GoMeRaiz();
}
//----------------------------------------------------------------------
//desativa sms
void MPreSetSmsOff(void)
{
    SetModoSms(SmsModoOff);
    GoMeRaiz();
}
//----------------------------------------------------------------------
//ativa somente sms
void MPreSetSmsOnly(void)
{
    SetModoSms(SmsModoOnly);
    GoMeRaiz();
}
//----------------------------------------------------------------------
//ativa monitoramento
void MPreSetMoniOn(void)
{
    SetModoMoni(MoniModoOn);
    GoMeRaiz();
}
//----------------------------------------------------------------------
//desativa monitoramento
void MPreSetMoniOff(void)
{
    SetModoMoni(MoniModoOff);
    GoMeRaiz();
}
//----------------------------------------------------------------------
//menus do sistema V4
const MENUS MenuV4[NumMenus]=
{
	//n�vel     rotina no meu       rotina pr� executada
	//fixos
    {NivNone,	&MPrincipal,		&MPreNone},//fixo menu raiz do sistema
	{NivNone,	&MNivelSelect,		&MPreNone},//fixo sele��o de n�vel de programa��o
    //n�veis de 1 d�gito (1, 2, 3, 4, 5 e 9)
    //discador
    {1,		&MProgTelef,        &MPreProgTelefPos1},//cadastra-apaga telefone 1
    {2,		&MProgTelef,        &MPreProgTelefPos2},//cadastra-apaga telefone 2
    {3,		&MProgTelef,        &MPreProgTelefPos3},//cadastra-apaga telefone 3
    {4,		&MProgTelef,        &MPreProgTelefPos4},//cadastra-apaga telefone 4
    {5,		&MProgTelef,        &MPreProgTelefPos5},//cadastra-apaga telefone 5
    {9,		&MNone,             &MPreTestTelef},//teste de todos os cadastrados
    //n�veis de 2 d�gitos (6x, 7x e 8x)
    {69,		&MNone,             &MPreApagaAllTel},//apaga todos os telefones
    //pgm
    {80,        &MNone,             &MPreSetPgmPls},//habilita a pgm como pulso
    {81,        &MNone,             &MPreSetPgmRet},//habilita a pgm como reten��o
    {82,        &MSetPgmTPls,       &MPreNone},//cadastra tempo de pgm em segundos
    {83,		&MNone,             &MPreTestPgm},//comanda a pgm
    //sms
    {84,        &MNone,             &MPreSetSmsOn},//ativa sms
    {85,        &MNone,             &MPreSetSmsOff},//desativa sms
    {86,        &MNone,             &MPreSetSmsOnly},//ativa somente sms
    //monitoramento
    {87,        &MNone,             &MPreSetMoniOn},//ativa monitoramento
    {88,        &MNone,             &MPreSetMoniOff},//desativa monitoramento
    //master reset
    {89,		&MMasterReset,      &MPreMasterReset}//apaga todas configura��es confirma com y
};

//fim do arquivo
