//GerenciadorGprs_v1.c
#include "GerenciadorGprs_v1.h" /* Ger�nciamento das tarefas do sistema Gprs        */
#include "derivative.h"         /* include peripheral declarations                  */
#include "Serial232_v2.h"       /* inteface e rotinas da comunica��o serial         */
#include "ATStateManager_v1.h"
#include "LedManager_v1.h"
#include "PilhaAT_v1.h"         /* processador de comandos AT                       */
#include "DiscadorManager.h"
#include "EmulaEEprom.h"
#include "TrataPgm.h"
#include "TrataDsp.h"
#include "TickTimer.h"
#include "TrataPanico.h"
#include "TrataMoni.h"
#include "TrataPWM.h"

//comandos enviados ao m�dulo
const CAR8 At_At[]={"\r\nAT\r\n"};              //comando de aten��o
const CAR8 At_Ath[]={"ATH\r\n"};            //encerra liga��o
const CAR8 At_Echooff[]={"\r\nATE0\r\n"};       //desabilita o �co de caracteres
const CAR8 At_ModText[]={"\r\nAT+CMGF=1\r\n"};  //configura o processador para o m�do de comandos de texto
const CAR8 At_Colp[]={"\r\nAT+COLP=1\r\n"};
const CAR8 At_EnviaSmsP1[]={"\r\nAT+CMGS=\""};  //comando para enviar uma mensagem de sms
const CAR8 At_EnviaSmsP2[]={"\"\r"};          //finaliza��o
const CAR8 At_EnviaSmsP1_M2[]={"\r\nAT+CMGS="};  //comando para enviar uma mensagem de sms
const CAR8 At_EnviaSmsP2_M2[]={"\r"};          //finaliza��o
const CAR8 At_GetSinal[]={"\r\nAT+CSQ\r\n"};    //solicita o n�vel de sinal
const CAR8 At_AtdP1[]={"\r\nATD"};              //disca
const CAR8 At_AtdP2[]={";\r"};
const CAR8 At_CmgrP1[]={"\r\nAT+CMGR="};              //le sms
const CAR8 At_CmgrP2[]={"\r\n"};
const CAR8 At_CmgdaAll[]={"\r\nAT+CMGDA=\"DEL ALL\"\r\n"};//apaga todas
const CAR8 At_GeraDtmf[]={"\r\nAT+VTS=\"1,9,1,9\"\r\n"};
const CAR8 At_Cpin[]={"\r\nAT+CPIN?\r\n"};
//comandos recebidos do m�dulo
const CAR8 cmd_OK[]={"OK"};         //informa��o de sucesso
const CAR8 cmd_Cpin[]={"+CPIN"};
const CAR8 cmd_Call[]={"CALL"};     //Sistema prondo Call argumento Ready
const CAR8 cmd_Ready[]={"READY"};   //Sistema pronto Ready
const CAR8 cmd_Sms[]={"SMS"};     //Sistema pronto Sms argumento Ready
const CAR8 cmd_RING[]={"RING"};     //recebe liga��o
const CAR8 cmd_CSQ[]={"+CSQ"};      //n�vel de sinal recebido
const CAR8 cmd_NO[]={"NO"};//No Dialtone, No Carrier
const CAR8 cmd_BUSY[]={"BUSY"};
const CAR8 cmd_CMTI[]={"+CMTI"};
const CAR8 cmd_CMGR[]={"+CMGR"};
const CAR8 cmd_CMS[]={"+CMS"};
const CAR8 cmd_ERROR[]={"ERROR"};

//comandos recebidos do usu�rio
const CAR8 cmd_PGM[]={"PGM"};//comandar a pgm

//alerta que aparece no terminal SMS
const CAR8 sms_DISP[]={"Alerta de Disparo!"};
const CAR8 sms_PANICO[]={"Alerta de Panico!"};
const CAR8 sms_TESTE[]={"Teste de Memoria"};
const CAR8 sms_MARCA[]={"\r\rDG5\rDiscador GSM Compatec"};

//globais
U8 AuxAtBoot;   //contador do n�mero limite de tentativas de sincroniza��o
//e contar o tempo de discagem

//para a verifica��o do caracter de terminal aberto
extern U8 BufSerial[TamanDoBuf];

U8 NivelRec;//n�vel de sinal de recep��o
void RotSinalRec(CAR8 *Param,U8 *Apont)
{
    U16 NivSinal;//armazena o n�vel de sinal

    if(ArgumentToInt(0,&NivSinal,Param,Apont))
    {
        NivelRec=(U8)NivSinal;
        //99 n�o conhecido ou n�o detect�vel
        //0 -113 dBm ou menos
        //1 -111 dBm
        //2...30 -109... -53 dBm
        //31 -51 dBm ou mais        
        LedErrGsm=LedOff;
        //superior
        //apagado   lento   r�pido  ligado
        //-------   17-20   21-24   25-acima

        //inferior
        //apagado   lento   rapido  ligado
        //-------   5-8     9-12    13-16
        
        if(NivelRec<5 || NivelRec==99)
        {
            LedErrGsm=LedFlash;
            LedSinalSup=LedOff;
            LedSinalInf=LedOff;
        }
        else if(NivelRec<9)
        {
            LedSinalSup=LedOff;
            LedSinalInf=LedAlterna;
        }
        else if(NivelRec<13)
        {
            LedSinalSup=LedOff;
            LedSinalInf=LedFlash;
        }
        else if(NivelRec<17)
        {
            LedSinalSup=LedOff;
            LedSinalInf=LedOn;
        }
        else if(NivelRec<21)
        {
            LedSinalSup=LedAlterna;
            LedSinalInf=LedOn;
        }
        else if(NivelRec<25)
        {
            LedSinalSup=LedFlash;
            LedSinalInf=LedOn;
        }
        else
        {
            LedSinalSup=LedOn;
            LedSinalInf=LedOn;
        }
    }else{
        //erro ao ler o n�vel de sinal
        LedErrGsm=LedFlash;
        LedSinalSup=LedOff;
        LedSinalInf=LedOff;
    }
}

void SetModoSms(U8 Modo)
{
    WriteEEprom(eModoSms,Modo);
}

U8 TestDisc=0;//setado inicia o teste de discagem
U8 PanicoDisc=0;//setado efetua discagem
U8 DspDisc=0;//setado efetua discagem
U8 LockPanico=0;
U8 LockDsp=0;
U8 ContCiclDisc;//conta os ciclos de discagem e de sms
U8 DiscAtual;//salva a mem�ria atual que est� sendo discada ou enviado sms
//envia para o terminal o n�mero configurado na posi��o da mem�ria
//serve para a tarefa de sms e discador
//antes de chamar a tarefa deve ser verificado se a pos���o possui n�mero cadastrado
//se n�o possuir simplesmente n�o � gerada a discagem
void GeraDiscTel(void)
{
    U8 i;
    U8 Digit;

    if(DiscAtual>=DiscNumMem)
    {
        return;
    }
    if(DiscGetDigito(DiscAtual,0)!=DigitNull)//verifica se possui n�mero na mem�ria
    {
        for(i=0;i<DiscMaxDigit;i++)
        {
            Digit=DiscGetDigito(DiscAtual,i);//busca o caracter atual
            if(Digit==DigitNull)//fim da mem�ria cadastrada
            {
                break;
            }
            SCISendInt(Digit);
        }
    }
}

void SmsAlerMsg(void)
{
    if(TestDisc)
    {
        SCISendString((CAR8 *)sms_TESTE);
    }
    else if(PanicoDisc)
    {
        SCISendString((CAR8 *)sms_PANICO);
    }
    else
    {
        SCISendString((CAR8 *)sms_DISP);
    }
    SCISendString((CAR8 *)sms_MARCA);
}

U8 RingRcvd;
void RotProcRing(CAR8 *Param,U8 *Apont)
{
    (void)Param;
    (void)Apont;//n�o s�o utilizadas
    RingRcvd=1;
}

U8 CpinReadyRcvd;
void RotProcCpinReady(CAR8 *Param,U8 *Apont)
{
    if(ComparaArgumentString(Param,Apont,0,(CAR8*)cmd_Ready))
    {
        CpinReadyRcvd=1;
    }
    else
    {
        CpinReadyRcvd=2;
    }
}

U8 CallReadyRcvd;
void RotProcCallReady(CAR8 *Param,U8 *Apont)
{
    RemCmd(cmd_Call);
    if(ComparaArgumentString(Param,Apont,0,(CAR8*)cmd_Ready))
    {
        CallReadyRcvd=1;
    }
    else
    {
        CallReadyRcvd=2;
    }
}

U8 SmsReadyRcvd;
void RotProcSmsReady(CAR8 *Param,U8 *Apont)
{
    RemCmd(cmd_Sms);
    if(ComparaArgumentString(Param,Apont,0,(CAR8*)cmd_Ready))
    {
        SmsReadyRcvd=1;
    }
    else
    {
        SmsReadyRcvd=2;
    }
}

U8 No_BusyRcvd;
void RotProcNo_Busy(CAR8 *Param,U8 *Apont)
{
    (void)Param;
    (void)Apont;//n�o s�o utilizadas
    No_BusyRcvd=1;
}

//recebe OK 
//usado para detectar chamada realizada
U8 OkRcvd;
void RotProcOkRcvd(CAR8 *Param,U8 *Apont)
{
    (void)Param;
    (void)Apont;//n�o s�o utilizadas
    OkRcvd=1;
}

//recebeu SMS
U8 SmsRcvd;
U16 SmsIndex;
void RotProcCmtiRcvd(CAR8 *Param,U8 *Apont)
{
    //mem (str)
    //index (int)
    //+CMTI: "SM",1
    if(ArgumentToInt(1,&SmsIndex,Param,Apont))
    {
        SmsRcvd=1;
    }
}

U8 BuffCmgr[30];
U8 CmgrRcvd;
void RotProcCmgrRcvd(CAR8 *Param,U8 *Apont)
{
    U8 QuantBytes;
    if(ArgumentToString(1,(CAR8*)BuffCmgr,&QuantBytes,30,Param,Apont))
    {
        CmgrRcvd=1;
    }
}

void GeraDiscCmgr(void)
{
    SCISendString((CAR8*)BuffCmgr);
}

U8 PgmRcvd;
void RotPgm(CAR8 *Param,U8 *Apont)
{
    (void)Param;
    (void)Apont;//n�o s�o utilizadas
    PgmRcvd=1;//seta o flag
    AcionaPgm();
}

void SmsPgmMsg(void)
{
    U8 ModoLido;
    U16 TempoLido;
    U8 ModoMoni;

    ModoMoni=ReadEEprom(eModoMoni);
    TempoLido=ReadEEpromInt(eTempoPgm);
    ModoLido=ReadEEprom(eModoPgm);
    
    if(ModoMoni==MoniModoOn)
    {
        if(StatusMoni())
        {
            SCISendString("Central Armada");
        }else{
            SCISendString("Central Desarmada");
        }
    }else{
        if(ModoLido==PgmModoRet)
        {
            if(REL_PGM)
            {
                SCISendString("PGM Ligada");
            }else{
                SCISendString("PGM Desligada");
            }
        }else{
            SCISendString("PGM Acionada por ");
            SCISendInt(TempoLido);
            if(TempoLido==1)SCISendString(" Segundo");
            else SCISendString(" Segundos");
        }
    }
    SCISendString((CAR8 *)sms_MARCA);
}

U8 GerarSirene=0;
U8 EstadoSirene=0;

TickS TDiscagem;
TickS TWaitCR;
TickS TNivSin;

//rotina de tratamento da pilha gsm
//deve ser chamada a cada 100ms loop principal
void ThreadPilhaAtMaster(void)
{
	unsigned char TempPilha;
    TempPilha=EstadoGsm;

    if(StatusPanico())//permanece monitorando continuamente
    {
        if(!LockPanico){
            LockPanico=1;
            PanicoDisc=1;//seta o flag apenas 1 vez a cada acionamento
        }
    }else{
        LockPanico=0;
    }
    if(StatusDsp())//permanece monitorando continuamente
    {
        if(!LockDsp){
            LockDsp=1;
            DspDisc=1;//seta o flag apenas 1 vez a cada acionamento
        }
    }else{
        LockDsp=0;
    }

	switch(TempPilha)
	{
		case GsmDelay://1 condi��o obrigat�ria
        	GsmTrataDelay();//chamada obrigat�ria
		break;

	    case GsmSendSms://2 condi��o obrigat�ria
			if(BufSerial[0]=='>')//terminal aberto
			{
                //chama a rotina que coloca a informa��o no terminal
                GprsRotinaEnviar();
				SCISendByte(26);//este � o ESC ou <SUB>
				//aguarda a confirma��o de mensagem enviada (+CMGS: depois OK)
				//durante 10 segundos se a mensagem n�o for enviada (+CMS ERROR:)
				//vai para a rotina que cancela o envio de sms
				//se for sms de confirma��o de comando e ocorrer erro
				//tem que ir pra outra rotina e n�o a GsmCancellSms
			    EsperaComandoGsm(cmd_OK,Min1,GprsRotOkPac,GsmCancellSms);
                //insta comando que executa mesma tarefa que a superior
                AddCmd(cmd_CMS,RotProcSimplGsm);
                //caso retorne erro executa mesma tarefa que a superior
                AddCmd(cmd_ERROR,RotProcSimplGsm);
			}
			else//n�o foi aberto o terminal
			{
				EstadoGsm=GsmFalhaComu;//Desconecta do servidor
			}
	    break;
        
        case GsmOffModulo://reinicializa��o do m�dulo
            LedSinalSup=LedOff;
            LedSinalInf=LedOff;
            LedErrGsm=LedOn;
            LedDisc=LedOff;
            NivelRec=0;//inicializa
            TermClearAllAtCommands();//remove todas as rotinas da pilha de processamento AT
            GerarSirene=0;
            StopPWM();
            //desliga o m�dulo por comando
            if(MOS_GPRS)//verifica se o m�dulo est� ligado ent�o o desliga
            {
                PWR_KEY_GPRS=1;//habilita o pino de comando do m�dulo
                GsmDelayRot(Seg3,GsmEndOffModulo);//gera um delay e vai para a rotina que para o desligamento
            }
            else
            {
                EstadoGsm=GsmEndOffModulo;//se j� estiver desligado simplesmente encaminha para a pr�xima rotina
            }
        break;

        case GsmEndOffModulo://encerra o comando de reinicializa��o
            PWR_KEY_GPRS=0;//desabilita o pino de comando       
            GsmDelayRot(Seg3,GsmCheckOffModulo);//gera um delay para a verifica��o de desligamento
        break;
        
        case GsmCheckOffModulo://verifica��o de desligamento
            GsmDelayRot(Seg2,GsmHardOfModulo);//aguarda 2 segundos e desliga a alimenta��o do m�dulo
        break;
        
	    case GsmHardOfModulo://desalimenta f�sicamente o m�dulo
	        MOS_GPRS=0;//corta a alimenta��o
	        GsmDelayRot(Seg5,GsmHardOnModulo);//aguarda 5 segundos para realimentar
	    break;
	    
	    case GsmHardOnModulo://alimenta f�sicamente o m�dulo
	        MOS_GPRS=1;//restaura a alimenta��o do m�dulo
	        GsmDelayRot(Seg2,GmsPwrOnModulo);//gera um delay de 2 segundos depois vai para a rotina de alimenta��o  
	    break;
        
        case GmsPwrOnModulo://alimenta��o do m�dulo
            CpinReadyRcvd=0;
            //adiciona Call Ready
            CallReadyRcvd=0;
            SmsReadyRcvd=0;
            RingRcvd=0;
            PgmRcvd=0;
            AddCmd(cmd_Cpin,RotProcCpinReady);
            AddCmd(cmd_Call,RotProcCallReady);
            AddCmd(cmd_Sms,RotProcSmsReady);
            //acionamento da pgm
            AddCmd(cmd_PGM,RotPgm);
            //ring in
            AddCmd(cmd_RING,RotProcRing);
            //n�vel de sinal
            AddCmd(cmd_CSQ,RotSinalRec);
            //erro de chamada
            AddCmd(cmd_NO,RotProcNo_Busy);
            AddCmd(cmd_BUSY,RotProcNo_Busy);
            AddCmd(cmd_CMTI,RotProcCmtiRcvd);
            AddCmd(cmd_CMGR,RotProcCmgrRcvd);
            PWR_KEY_GPRS=1;//habilita a tecla de comando do m�dulo
            GsmDelayRot(Seg2,GsmEndOnModulo);//gera um delay de 2 segundo e vai para a rotina de verifica��o de alimenta��o   
        break;
        
        case GsmEndOnModulo://encerra o comando de inicializa��o
            PWR_KEY_GPRS=0;//desabilita o pino de comando       
            GsmDelayRot(Seg2,GsmCheckOnModulo);//gera um delay para a verifica��o se ligou        
        break;

        case GsmCheckOnModulo://verifica se o m�dulo foi ligado
            LedErrGsm=LedAlterna;
            GsmDelayRot(Seg2,GsmAutoBaud);//aguarda 2 segundo para o processo de boot do m�dulo
            AuxAtBoot=0;//utilizado para a contagem de n�mero de tentativas
            //envia os comandos AT espera por OK
            GsmCmdAnt=(U8*)cmd_OK;//carraga o registrador com o comando a ser esperado
            GsmCmdRec=GsmSetupEcho;//carrega a rotina se der sucesso
            AddCmd(cmd_OK,RotProcSimplGsm);//instalamos o comando esperado
        break;

        case GsmAutoBaud:
            if(AuxAtBoot>=15)//caso tenha se tentado 15 vezes sem sucesso
            {
                RemCmd(cmd_OK);//removemos o comando da pilha (no estado Rreboot todos os comandos s�o removidos)
                EstadoGsm=GsmFalhaComu;//gera um delay de 1 seg e volta ao in�cio
            }
            else
            {
                SCISendString((CAR8 *)At_At);//envia o comando AT
                GsmDelayRot(Mili500,GsmAutoBaud);//gera um delay de 500ms e retorna para mesma rotina
                AuxAtBoot++;//incrementa a quantidade de tentativas
            }
        break;

		case GsmSetupEcho:
            //envia o comando para desabilitar o �co
			//espera durante 500ms se n�o receber o comando OK reinicia
			EnviaComandoGsm(At_Echooff,cmd_OK,Mili500,GsmWaitCheckCpin,GsmFalhaComu);
		break;

        case GsmWaitCheckCpin:
            //aguarda um instante para o modem inicializar o Sim
            GsmDelayRot(Seg5,GsmCheckCpin);
        break;
        
        case GsmCheckCpin:
            TickSet(20000,&TWaitCR);//utiliza para aguardar o Cpin Ready
            SCISendString((CAR8 *)At_Cpin);///envia o comando AT
            EstadoGsm=GsmWaitCpinReady;
        break;
        
        case GsmWaitCpinReady:
			//aguarda pela informa��o de Cpin Ready do m�dulo
            if(CpinReadyRcvd==1)
            {
                //aguarda um instante para o modem inicializar por completo
                GsmDelayRot(Seg5,GsmSetupModText);
                break;
            }
            if(TickExpired(&TWaitCR) || CpinReadyRcvd==2)
            {
                EstadoGsm=GsmFalhaComu;
            }
        break;
        
		case GsmSetupModText:
			//envia o comando para habilitar o m�do texto
			//espera durante 500ms se n�o receber o comando OK reinicia
			EnviaComandoGsm(At_ModText,cmd_OK,Mili500,GsmSetupColp,GsmFalhaComu);
        break;

	    case GsmSetupColp:
            TickSet(40000,&TWaitCR);//utiliza para aguardar o Call Ready
			//configura para que s� gere o OK se a liga��o for atendida
			EnviaComandoGsm(At_Colp,cmd_OK,Mili500,GsmWaitCallReady,GsmFalhaComu);
	    break;

		case GsmWaitCallReady:
			//aguarda pela informa��o de Call Ready do m�dulo
            if(CallReadyRcvd==1)
            {
                TickSet(40000,&TWaitCR);//utiliza para aguardar o Sms Ready
                EstadoGsm=GsmWaitSmsReady;
                break;
            }
            if(TickExpired(&TWaitCR) || CallReadyRcvd==2)
            {
                EstadoGsm=GsmFalhaComu;
            }
		break;

        case GsmWaitSmsReady:
			//aguarda pela informa��o de Sms Ready do m�dulo
            if(SmsReadyRcvd==1)
            {
                //aguarda um instante para o modem inicializar por completo
                GsmDelayRot(Seg5,GsmOnline);
                TickSet(10000,&TNivSin);//teste de sinal a cada 10 segundos
                break;
            }
            if(TickExpired(&TWaitCR) || SmsReadyRcvd==2)
            {
                EstadoGsm=GsmFalhaComu;
            }
        break;
        
	    case GsmClearSms://apaga todas as mensagens
	        EnviaComandoGsm(At_CmgdaAll,cmd_OK,Seg5,GsmOnline,GsmFalhaComu);
	    break;
	    
	    case GsmOnline:
            if(RingRcvd)//est� recebendo chamada
            {
                RingRcvd=0;//apaga o flag
                //encerra a liga��o
                EnviaComandoGsm(At_Ath,cmd_OK,Seg5,GsmOnline,GsmFalhaComu);
                break;
            }
            if(SmsRcvd)
            {
                SmsRcvd=0;
                CmgrRcvd=0;//apaga o flag informando que recebeu o cabe�alho da mensagem
                EsperaComandoGsm(cmd_OK,Seg10,GsmClearSms,GsmFalhaComu);
                //envia o comando para ler a mensagem
                SCISendString((CAR8 *)At_CmgrP1);
                SCISendInt(SmsIndex);
                SCISendString((CAR8 *)At_CmgrP2);
                break;
            }
            if(CmgrRcvd)
            {
                CmgrRcvd=0;//apaga o flag
                if(PgmRcvd)//comando de pgm foi recebido
                {
                    PgmRcvd=0;//apaga o flag
                    GsmDelayRot(Seg3,GsmSendSmsPgm);//aguarda 3 segundos para responder
                }
                break;
            }
            if(TestDisc | PanicoDisc)
            {
                ContCiclDisc=(DiscNumDisc-1);//disca apenas 1 vez para cada registro
                DiscAtual=0;//sms e discagem
                if(ReadEEprom(eModoSms)!=SmsModoOff)//come�a pelas SMS
                {
                    EstadoGsm=GsmProxSms;
                }
                else
                {
                    EstadoGsm=GsmProxDisc;
                }
                break;
            }
            else if(DspDisc)
            {
                ContCiclDisc=0;
                DiscAtual=0;//sms e discagem
                if(ReadEEprom(eModoSms)!=SmsModoOff)//come�a pelas SMS
                {
                    EstadoGsm=GsmProxSms;
                }
                else
                {
                    EstadoGsm=GsmProxDisc;
                }
                break;
            }
            //monitora o n�vel de sinal a cada 10 segundos
            if(TickExpired(&TNivSin))
            {
                TickRestart(&TNivSin);//pr�ximo daqui a 10 segundos ignora o atrazo
                EnviaComandoGsm(At_GetSinal,cmd_OK,Seg2,GsmOnline,GsmFalhaComu);
                break;    
            }
            if(CpinReadyRcvd!=1)
            {
                EstadoGsm=GsmFalhaComu;
            }
	    break;
        
        case GsmSendSmsPgm:
            //envia o comando de resposta
            EnviaSmsM2(&SmsPgmMsg,&GeraDiscCmgr,GsmOnline);
        break;

	    case GsmCancellSms:
            RemCmd(cmd_CMS);//fun��o especial da aplica��o
            RemCmd(cmd_ERROR);//fun��o especial da aplica��o
	        //cancela o envio de sms pois ocorreu erro ao enviar para algum n�mero
            if(ReadEEprom(eModoSms)==SmsModoOnly)
            {
                GsmDelayRot(Mili500,GsmOnline);
                TestDisc=0;
                PanicoDisc=0;
                DspDisc=0;
            }
            else
            {
                GsmDelayRot(Mili500,GsmProxDisc);
                if(PanicoDisc || TestDisc)
                {
                    ContCiclDisc=(DiscNumDisc-1);//disca apenas 1 vez para cada registro
                }
                else
                {
                    ContCiclDisc=0;
                }
                DiscAtual=0;//sms e discagem
            }
	    break;
	    
	    case GsmDelayProxSms:
	        DiscAtual++;//incrementa a posi��o de envio de sms
	        GsmDelayRot(Mili500,GsmProxSms);//agurda um instante para enviar o pr�ximo pacote
	    break;
	    
	    case GsmProxSms://estado inicial
	        if(DiscAtual>=DiscNumMem)
	        {
                if(ReadEEprom(eModoSms)==SmsModoOnly)
                {
                    GsmDelayRot(Mili500,GsmOnline);
                    TestDisc=0;
                    PanicoDisc=0;
                    DspDisc=0;
                }
                else
                {
                    GsmDelayRot(Mili500,GsmProxDisc);
                    if(PanicoDisc || TestDisc)
                    {
                        ContCiclDisc=(DiscNumDisc-1);//disca apenas 1 vez para cada registro
                    }
                    else
                    {
                        ContCiclDisc=0;
                    }
                    DiscAtual=0;//sms e discagem
                }
                break;//encerra
	        }
            if(DiscGetDigito(DiscAtual,0)==DigitNull)//verifica se possui n�mero na mem�ria
            {
                DiscAtual++;//incrementa para verificar o pr�ximo
                break;//enecrra para verificar a pos���o do pr�ximo
            }
            //envia mensagem para o DiscAtual
            EnviaSms(&SmsAlerMsg,&GeraDiscTel,GsmDelayProxSms);
        break;

	    case GsmDelayProxDisc:
	        DiscAtual++;//incrementa a posi��o de discagem
	        GsmDelayRot(Mili500,GsmProxDisc);//agurda um instante para enviar o pr�ximo pacote
        break;
	    
	    case GsmProxDisc:
	        if(DiscAtual>=DiscNumMem)
	        {
                DiscAtual=0;//sms e discagem
                ContCiclDisc++;//incrementa os ciclos
                if(ContCiclDisc>=DiscNumDisc
                || (!StatusDsp() && DspDisc))
                {
                    GsmDelayRot(Mili500,GsmOnline);
                    TestDisc=0;
                    PanicoDisc=0;
                    DspDisc=0;
                    break;
                }
                GsmDelayRot(Mili500,GsmProxDisc);
                break;//encerra
	        }
            if(DiscGetDigito(DiscAtual,0)==DigitNull)//verifica se possui n�mero na mem�ria
            {
                DiscAtual++;//incrementa para verificar o pr�ximo
                break;//enecrra para verificar a pos���o do pr�ximo
            }
            //efetua a discagem
            LedDisc=LedOn;
            SCISendString((CAR8 *)At_AtdP1);
            GeraDiscTel();
            SCISendString((CAR8 *)At_AtdP2);
            EstadoGsm=GsmWaitCon;
            No_BusyRcvd=0;//apaga o flag de erro
            OkRcvd=0;
            TickSet((U16)DiscTempoDisc*1000,&TDiscagem);//conta o tempo de discagem
            AddCmd(cmd_OK,RotProcOkRcvd);
	    break;

	    case GsmWaitCon://aguarda atender a liga��o
	        if(No_BusyRcvd || TickExpired(&TDiscagem))
	        {
	            GsmDelayRot(Mili500,GmsEndDisc);
                RemCmd(cmd_OK);
	            break;
	        }
            if(OkRcvd)
            {
                LedDisc=LedFlash;
                GerarSirene=1;
                GsmDelayRot(Seg1,GsmEmDisc);
                RemCmd(cmd_OK);
                break;
            }
	    break;

	    case GmsEndDisc:
            LedDisc=LedOff;
	        //envia o comando para encerrar a liga��o sempre antes de discar novamente
	        EnviaComandoGsm(At_Ath,cmd_OK,Seg5,GsmDelayProxDisc,GsmFalhaComu);
	    break;
	    
	    case GsmEmDisc:
	        if(No_BusyRcvd || TickExpired(&TDiscagem))
	        {
                GerarSirene=0;
                StopPWM();
                GsmDelayRot(Mili500,GmsEndDisc);
	            break;
	        }
	    break;
		
		case GsmFalhaComu:
			GsmDelayRot(Seg1,GsmOffModulo);//reinicia
		break;
		
		default://desconhecido
		    GsmInitTask();
	    break;
	}//fim dos estados
    if(GerarSirene)
    {  
        if(TestDisc)
        {
            if(EstadoSirene>4){//100ms on 400ms off
                EstadoSirene=0;
                StartPWM(FreqDispHi);
            }else{
                StopPWM();
            }
        }else if(PanicoDisc)
        {
            if(EstadoSirene>1){//100ms on 100ms off
                EstadoSirene=0;
                StartPWM(FreqDispHi);
            }else{
                StopPWM();
            }
        }else{//DspDisc
            if(EstadoSirene>3){//200ms on 200ms on
                EstadoSirene=0;
            }
            if(EstadoSirene>1)
            {
                StartPWM(FreqDispHi);
            }else{
                StartPWM(FreqDispLo);
            }
        }
        EstadoSirene++;
    }else{
        EstadoSirene=0;
    }
}//fim da thread

//fim do arquivo
