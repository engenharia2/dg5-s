#ifndef TRATA_MONI_H
#define TRATA_MONI_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

#define ENT_MONI     PB_IDR_IDR5
#define DIR_ENT_MONI PB_DDR_DDR5
#define CR1_ENT_MONI PB_CR1_C15

void InitMoni(void);

/*
    //tarefa a cada 10ms no loop principal
    (void)InstalaRotina(&TarefaTrataMoni,10,MODO_LOOP_TEMPO);
*/
void TarefaTrataMoni(void);

U8 StatusMoni(void);

#endif
//fim do arquivo
