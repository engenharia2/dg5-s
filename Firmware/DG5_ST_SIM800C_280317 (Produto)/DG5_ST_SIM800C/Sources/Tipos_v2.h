//Defini��o dos tipos de vari�veis para o microcontrolador
//Vers�o 2
#ifndef TIPOS_V2
#define TIPOS_V2

typedef unsigned long U32;//32 bits sem sinal
typedef unsigned short U16;//16 bits sem sinal (32bits Coldfyre)
//typedef unsigned int U16;//16 bits sem sinal (8bits HCS08)
typedef unsigned char U8;//8 bits sem sinal
typedef const unsigned char U8FLASH;//8 bits constante em FLASH
typedef char PTR;//apontador geral para arquitetura de 8 a 32 bits
typedef char CAR8;//caracteres ASCII
typedef int INT;//inteiro

#ifndef TRUE 
    #define TRUE  1
#endif
#ifndef FALSE
    #define FALSE 0
#endif

//#pragma MESSAGE DISABLE C1106

//fim do arquivo
#endif
