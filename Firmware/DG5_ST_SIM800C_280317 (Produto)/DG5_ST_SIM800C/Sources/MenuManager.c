//MenuManager.c

#include "MenuManager.h"
#include "TrataTeclado.h"
#include "TrataBuzzer.h"
#include "derivative.h" /* include peripheral declarations */

TEC Tec;//estrutura teclado

//rotina vazia para n�o executar a pre rotina
void MPreNone(void)
{
    //n�o faz nada
    __no_operation();
}

void GoMeSelNiv(void)
{
	TeC.Nivel=MeSelecNiv;
	TeC.Apont=0;//limpa o buffer de d�gitos
}

void GoMeRaiz(void)
{
	TeC.Nivel=MeRaiz;
	TeC.Apont=0;//limpa o buffer de d�gitos
	Tec.Timer=0;
}

//altera��o realizada externamente a uma tecla efetua a atualiza��o do menu
void UpdateMenu(void)
{
    //carrega o timeout de programa��o
    TeC.Timer=TimeoutSetup;

	//atualiza��o do teclado incorporado
	//verifica se possui algum beep para gerar
	if(Tec.Beep)
	{
        //encaminha para o tratamento de beeps
        TipoBeep=GetTipoBeep(Tec.Beep);
        ContBeep=GetNumBeeps(Tec.Beep);
		Tec.Beep=0;//ap�s enviar limpa a gera��o de beeps
	}
}

void MNivelSelect(void)
{
	U8 SeqDigit;//armazena a sequ�ncia digitada
	U8 i;//varre todos os menus
	U8 MenFind=0;//n�o encontrado

	//efetua a sele��o do n�vel desejado
	if(TeC.Digit==DigProg)//tecla de cancelamento
	{
		TeC.Nivel=MeRaiz;//volta ao n�vel raiz
		TeC.Apont=0;//limpa o buffer de d�gitos
	}
	if(TeC.Apont>=2//recebidos 2 d�gitos para sele��o de n�vel
	||TeC.Digit==DigUm
	||TeC.Digit==DigDois
	||TeC.Digit==DigTres
	||TeC.Digit==DigQuatro
	||TeC.Digit==DigCinco
	||TeC.Digit==DigNove)
	{
		if(TeC.Apont>=2)
		{
    		//verifica o n�vel
    		SeqDigit=TeC.Buf[0].Nibble.hsb*10;//converte para uma int o valor das 2 teclas
    		SeqDigit+=TeC.Buf[0].Nibble.lsb;
		}
		else
		{
    		SeqDigit=TeC.Buf[0].Nibble.hsb;
		}
        TeC.Apont=0;//limpa o buffer de d�gitos
		if(SeqDigit!=NivNone)//se o c�digo digitado n�o for o NivNone
		{
			//verifica se encontra o menu
			for(i=0;i<NumMenus;i++)
			{
				if(MenuV4[i].Codigo==SeqDigit)//se o c�d for igual
				{
					MenFind=1;//localizado o menu
					TeC.Beep=Beep2Curtos;
					TeC.Nivel=i;//desvia o programa
					//chama a rotina de pr� configura��o
					//Criamos um ponteiro para uma fun��o void
					{
    					TipoMenus * Comando;
    					//carregamos o ponteiro com o endere�o da rotina
    					Comando=MenuV4[TeC.Nivel].PreRot;
    					//chama a rotina
    					Comando();
					}
					break;//encerra o la�o de repeti��es
				}
			}
		}
		if(!MenFind)//caso n�o tenha localizado
		{
			TeC.Beep=Beep1Longo;//beep de erro
			GoMeRaiz();
		}
	}
}

//inicializa o teclado
void InitMenu(void)
{
    Tec.Apont=0;
    Tec.Nivel=MeRaiz;
	Tec.Timer=0;
	Tec.Beep=0;
}

//Timeout do Menu retorna ao root
//a cada 100ms no programa principal
void MenuTrataTimer(void)
{
    if(Tec.Timer)
    {
        Tec.Timer--;
        if(!Tec.Timer)
        {
        	//desliga os leds e para o alerta de erro e volta ao principal
        	Tec.Apont=0;
        	Tec.Nivel=MeRaiz;
        }
    }
}

//armazena no buffer um d�gito
void TecBufAddDigito(U8 Digito)
{
	Digito&=0x0F;//limpa o d�gito
	if(TeC.Apont>=(LimBufTec*2))//se a pilha estiver cheia protege contra acesso ilegal da mem�ria
	{
		TeC.Apont=0;//limpa o buffer
	}
	if(TeC.Apont%2)//� impar
	{
		TeC.Buf[TeC.Apont/2].Nibble.lsb=Digito;//salva segunda parte
	}
	else//� par
	{
		TeC.Buf[TeC.Apont/2].Nibble.hsb=Digito;//salva primeira parte
	}
	TeC.Apont++;//incrementa a pilha
}

//processador de d�gitos
//� chamado cada vez que a leitura do teclado recebe uma t�cla v�lida
void MenuTrataDigito(U8 Digito)
{
	if(Digito<=9)//armazena no buffer do 0 ao 9
	{
        TecBufAddDigito(Digito);
	}
	TeC.Digit=Digito;//armaena o d�gito atual
	//ent�o chama a rotina
	{
    	//Criamos um ponteiro para uma fun��o void
    	TipoMenus * Comando;
    	//carregamos o ponteiro com o endere�o da rotina
    	Comando=MenuV4[TeC.Nivel].Rotinas;
    	//chama a rotina
    	Comando();
    }
	UpdateMenu();
}
//fim do arquivo
