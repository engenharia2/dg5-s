//CopProcess.c

#include "CopProcess.h"
#include "derivative.h"         /* include peripheral declarations                  */

//COP configurado para estourar em 1024ms

#define CopLimit    1500//15 segundos
U16 CopCont=0;

//COP a cada 10ms na interrupção
void CopProcess(void)
{
    if(CopCont<CopLimit)
    {
        CopCont++;
        __RESET_WATCHDOG();//feeds the dog
    }
}

void ResetCpu(void)
{
    CopCont=CopLimit;
    for(;;)
    {
    }
}

//tarefa no loop principal a cada 1 seg
void CopPeriodic(void)
{
    CopCont=0;
}
//fim do arquivo
