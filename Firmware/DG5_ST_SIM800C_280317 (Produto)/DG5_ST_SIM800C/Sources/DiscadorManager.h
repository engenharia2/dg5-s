#ifndef DISCADOR_MANAGER_H
#define DISCADOR_MANAGER_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

#define DiscMaxDigit    22//m�ximo de d�gitos do discador (tem que ser par)
#define DiscNumMem      5//n�mero de mem�rias do discador deve ser igual a NumSet para discar todos setores
#define DiscNumDisc     3//quantidade de discagens para cada telefone
#define DiscTempoDisc   35//segundos por liga��o

//endere�o inicial dos telefones
#define AddrDisc        0
//0 + (22/2)*5 = 55

//caracteres especiais do discador
#define DigitNull   0x0F//indica mem�ria vazia e fim da mem�ria
void DiscProgramTelef(U8 Pos,U8 * Buf,U8 Compr);

U8 DiscGetDigito(U8 Pos,U8 Digit);
void DiscApagaTelef(U8 Pos);
void DiscApagaAllTelef(void);

#endif
//fim do arquivo
