#include "DriverLedsMtpx.h"
#include "derivative.h"

FlagLeds Leds;

void InitLedsMtpx(void)
{
    //Comentado para SWIMDelayedStartup
    //DIR_LED_MUX_1=1;
    //CR1_LED_MUX_1=1;
    //LED_MUX_1=0;
    
    DIR_LED_MUX_2=1;
    CR1_LED_MUX_2=1;
    LED_MUX_2=0;
    
    DIR_LED_MUX_3=1;
    CR1_LED_MUX_3=1;
    LED_MUX_3=0;

    Leds.u8Bits=0;
}

//a cada 10ms na interrupção
void UpdateLedsMtpx(void)
{
    static U8 EstagioMux=0;
    if(EstagioMux)
    {
        LED_MUX_2=1;
        LED_MUX_3=!Leds.sBits.led1;
        LED_MUX_1=!Leds.sBits.led3;
    }else{
        LED_MUX_2=0;
        LED_MUX_3=Leds.sBits.led2;
        LED_MUX_1=Leds.sBits.led0;
    }
    EstagioMux=!EstagioMux;
}
