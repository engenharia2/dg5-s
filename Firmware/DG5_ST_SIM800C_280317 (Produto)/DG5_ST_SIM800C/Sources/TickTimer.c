//TickTimer.c
/*
	Tick Timer
	Base de tempo para aplica��es embarcadas
	Joel Langer
	R00 23-05-10
	
	Adicionado volatile para a TickbaseClock resolvendo bug em HCS08 
*/
#include "TickTimer.h"
#include "derivative.h"       /* include peripheral declarations */

volatile U16 TickbaseClock;

void TickClockInit(void)
{
	TickbaseClock=0;
    //TIM4
    //16MHz/128 = 125 kHz/125 = 1kHz(1ms)
    TIM4_PSCR=0x07;//prescaler = 128
    TIM4_ARR=125;
    TIM4_EGR=0;
    TIM4_CR1=MASK_TIM4_CR1_CEN;
    TIM4_IER=MASK_TIM4_IER_UIE;
}
//inicializa o ponteiro de uma estrutura
//intervalo em ms
//intervalo m�ximo 64 segundos
void TickSet(U16 Intervalo,TickS * T)
{
	T->Intervalo=Intervalo;
	T->Start=TickbaseClock;
}
//reconfigura o intervalo
void TickChange(U16 Intervalo,TickS * T)
{
	T->Intervalo=Intervalo;
}
//reseta o timer sem perder a seq��ncia de intervalo
void TickReset(TickS * T)
{
	T->Start+=T->Intervalo;
}
//retorna 1 se o time expirou
U8 TickExpired(TickS * T)
{
	if((U16)(TickbaseClock - T->Start) >= T->Intervalo)
	{
		return(1);
	}
	return(0);
}
//reinicia o timer a partir do momento atual
//para n�o perder a periodicidade usar TickReset
void TickRestart(TickS * T)
{
	T->Start=TickbaseClock;
}
//deve ser chamada a cada 1ms na interrup��o
#pragma vector = TIM4_OVR_UIF_vector
__interrupt void TickApp(void)
{
    TIM4_SR_UIF=0;
	TickbaseClock++;//incrementa a base de tempo
}
//fim do arquivo
