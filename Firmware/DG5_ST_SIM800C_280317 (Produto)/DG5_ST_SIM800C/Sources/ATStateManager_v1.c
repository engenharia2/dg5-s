//ATStateManager_v1.c
//Ger�nciamento dos estados de comunica��o da pilha GSM
#include "ATStateManager_v1.h"
#include "GerenciadorGprs_v1.h" /* Ger�nciamento das tarefas do sistema Gprs        */
#include "PilhaAT_v1.h"         /* processador de comandos AT                       */
#include "Serial232_v2.h"//inteface e rotinas da comunica��o serial

//vari�veis globais da camada gsm
U8 EstadoGsm;           //m�quina de estados da comunica��o gsm
U16  GsmTimerDelay;     //timer para os delays da m�quina de estados
U8 GsmTimeutRot;	    //rotina que ser� chamada se ocorrer um timeout
U8 GsmCmdRec;		    //rotina que ser� chamada se for recebido um comando
//U8 GsmCmdRec2;		    //rotina que ser� chamada se for recebido um comando
U8 GsmCmdRecArgs[3];    //rotinas caso argumentos coincidirem
U8 *GsmCmdAnt;			//comando anterior instalado na pilha
U8 *GsmCmdAntArgs[3];	//endere�o dos argumentos a serem verificados
U8 GsmCmdErrArg;	    //comando recebido mas argumento inv�lido
U8 GsmRemovCmd;			//quando setado o timeout remove a rotina instalada
//envio de mensagem para o terminal
TipoRotina *GprsRotinaEnviar;   //rotina que ser� chamada
U8 GprsRotOkPac;                //sucesso ao enviar pacote

// inicializa��o o ger�nciador
void GsmInitTask(void)
{
	EstadoGsm=GsmBootPos;
	GsmTimerDelay=0;
	GsmTimeutRot=GsmBootPos;
	GsmCmdRec=GsmBootPos;
	GsmRemovCmd=0;
}

// ger�ncia as tarefas de delay
void GsmTrataDelay(void)
{
	if(GsmTimerDelay)GsmTimerDelay--;	//decrementa
	if(!GsmTimerDelay)					//caso timeout
	{
		EstadoGsm=GsmTimeutRot;			//vai para a rotina
		GsmTimeutRot=GsmBootPos;		//apaga por seguran�a
		if(GsmRemovCmd)					//caso estava se esperando um comando instalado na pilha
		{
			RemCmd(GsmCmdAnt);			//remove o comando da pilha
			GsmRemovCmd=0;				//desabilita a op��o de espera por timeout
			GsmCmdRec=GsmBootPos;		//apaga os flags por seguran�a
		}
	}
}

// rotina de delay com altera��o de tarefa no final
void GsmDelayRot(U16 Tempo,U8 Retorno)
{
	GsmTimerDelay=Tempo;
	if(Tempo)						//se o tempo for maior que 0 prepara o timer
    {
		GsmTimeutRot=Retorno;
		EstadoGsm=GsmDelayPos;
    }
	else							//se o tempo for 0 vai para a tarefa
    {
		EstadoGsm=Retorno;			//vai para a rotina
		GsmTimeutRot=GsmBootPos;	//apaga por seguran�a
    }
}

// rotina trata os comandos sem argumentos na pilha at
void RotProcSimplGsm(CAR8 *Param,U8 *Apont)
{
    (void)Param;
	(void)Apont;				//n�o s�o utilizadas
    RemCmd(cmd_CMS);//fun��o especial da aplica��o
    RemCmd(cmd_ERROR);//fun��o especial da aplica��o
	RemCmd(GsmCmdAnt);			//remove o comando da pilha (primeira a��o)
	GsmTimerDelay=0;			//zera a rotina de delay
	GsmTimeutRot=GsmBootPos;	//apaga por seguran�a
	GsmRemovCmd=0;				//desabilita a remo��o de comando
	EstadoGsm=GsmCmdRec;		//coloca a rotina a ser eceutada
	GsmCmdRec=GsmBootPos;		//apaga por seguran�a
}

/*// rotina trata os comandos sem argumentos na pilha at
void RotProcSimplGsm2(CAR8 *Param,U8 *Apont)
{
    (void)Param;
	(void)Apont;				//n�o s�o utilizadas
    RemCmd(cmd_CMS);//fun��o especial da aplica��o
    RemCmd(cmd_ERROR);//fun��o especial da aplica��o
	RemCmd(GsmCmdAnt);			//remove o comando da pilha (primeira a��o)
	GsmTimerDelay=0;			//zera a rotina de delay
	GsmTimeutRot=GsmBootPos;	//apaga por seguran�a
	GsmRemovCmd=0;				//desabilita a remo��o de comando
	EstadoGsm=GsmCmdRec2;		//coloca a rotina a ser eceutada
	GsmCmdRec=GsmBootPos;		//apaga por seguran�a
}*/

// envia um comando e espera por uma resposta ou timeout (n�o considera os argumentos)
void EnviaComandoGsm(const CAR8 *ComandoEnviar,const CAR8 *RespostaEsperada,U16 TempoLimiteEspera,U8 RotinaSeSucesso,U8 RotinaSeTimeout)
{
	GsmCmdAnt=(U8*)RespostaEsperada;			        //carraga o registrador com o comando a ser esperado
	GsmCmdRec=RotinaSeSucesso;							//carrega a rotina se der sucesso
	GsmDelayRot(TempoLimiteEspera,RotinaSeTimeout);		//instalamos o comando de timeout
	GsmRemovCmd=1;										//informa para a rotina de delay que tem que remover o comando se estourar o tempo
	AddCmd(RespostaEsperada,RotProcSimplGsm);			//instalamos o comando esperado
	SCISendString((CAR8*)ComandoEnviar);	                //enviamos o comando
}

// espera por uma resposta ou timeout (n�o considera os argumentos)
void EsperaComandoGsm(const CAR8 *RespostaEsperada,U16 TempoLimiteEspera,U8 RotinaSeSucesso,U8 RotinaSeTimeout)
{
	GsmCmdAnt=(U8*)RespostaEsperada;			        //carraga o registrador com o comando a ser esperado
	GsmCmdRec=RotinaSeSucesso;							//carrega a rotina se der sucesso
	GsmDelayRot(TempoLimiteEspera,RotinaSeTimeout);		//instalamos o comando de timeout
	GsmRemovCmd=1;										//informa para a rotina de delay que tem que remover o comando se estourar o tempo
	AddCmd(RespostaEsperada,RotProcSimplGsm);			//instalamos o comando esperado
}

// rotina trata os comandos com argumentos na pilha at
void RotProcArgsGsm(CAR8 *Param,U8 *Apont)
{
	U8 i;
	RemCmd(GsmCmdAnt);                  //removemos o comando da pilha (tem que ser a primeira a��o)
	EstadoGsm=GsmCmdErrArg;             //carrega o estado com o valor do argumento inv�lido
	for(i=0;i<3;i++)                    //efetua o loop de verifica��o dos argumentos
	{
		                                //compara o primeiro arumento
		                                //estamos repassando os endere�os param e apont
	                            	    //solicitando a compara��o do argumento i com o argumento do endere�o
		if(ComparaArgumentString(Param,Apont,0,(CAR8 *)GsmCmdAntArgs[i]))
		{
			                            //caso esteja de acordo com a argumento esperado
			EstadoGsm=GsmCmdRecArgs[i]; //muda o destino do programa
		}
	}
	GsmTimerDelay=0;			        //zera a rotina de delay
	GsmTimeutRot=GsmBootPos;	        //apaga por seguran�a
	GsmCmdRec=GsmBootPos;	    	    //apaga por seguran�a
	GsmRemovCmd=0;				        //desabilita a remo��o de comando
}

// espera por uma resposta com argumentos e timeout
// valores de entrada - comando esperado,tempo limite de espera,rotina se timeout,argumentos,rotinas se argumentos,
// rotina se nenhum argumento esperado foi recebido
// os argumentos n�o utilizados devem ser preenchidos com "" e 0
void EsperaComandoGsmArgs(const CAR8 *RespostaEsperada,U16 TempoLimiteEspera,U8 RotinaSeTimeout,
const CAR8 *Argumento1,const CAR8 *Argumento2,const CAR8 *Argumento3,
U8 RotinaSeSucesso1,U8 RotinaSeSucesso2,U8 RotinaSeSucesso3,U8 RotinaSeErrArg)
{
	GsmCmdAnt=(U8*)RespostaEsperada;           		//carraga o registrador com o comando a ser esperado
	GsmCmdRecArgs[0]=RotinaSeSucesso1;				//carrega a rotina se der sucesso 1
	GsmCmdRecArgs[1]=RotinaSeSucesso2;				//carrega a rotina se der sucesso 2
	GsmCmdRecArgs[2]=RotinaSeSucesso3;				//carrega a rotina se der sucesso 3
	GsmCmdErrArg=RotinaSeErrArg;					//carrega a rotina se n�o conferir com nenhum argumento
	GsmCmdAntArgs[0]=(U8*)Argumento1;           	//carrega o argumento 1
	GsmCmdAntArgs[1]=(U8*)Argumento2;           	//carrega o argumento 2
	GsmCmdAntArgs[2]=(U8*)Argumento3;           	//carrega o argumento 3
	GsmDelayRot(TempoLimiteEspera,RotinaSeTimeout);	//instalamos o comando de timeout
	GsmRemovCmd=1;									//informa para a rotina de delay que tem que remover o comando se estourar o tempo
	AddCmd(RespostaEsperada,RotProcArgsGsm);		//instalamos o comando esperado
}

//prepara para enviar um pacote para o servidor
//a rotina se erro ou se timeout � a desconex�o da rede tcp
void EnviaSms(TipoRotina *RotinaEnvia,TipoRotina *RotinaGeraTelef,U8 RotinaSeSucesso)
{
	SCISendString((CAR8*)At_EnviaSmsP1);        //envia o comando para abertura do terminal
    RotinaGeraTelef();
	SCISendString((CAR8*)At_EnviaSmsP2);        //envia o comando para abertura do terminal
	GsmDelayRot(Mili500,GsmSendSms);   //gera um delay para a verifica��o de abertura do terminal
	GprsRotinaEnviar=RotinaEnvia;           //carrega o endere�o do pacote a ser enviado
	GprsRotOkPac=RotinaSeSucesso;           //rotina se sucesso ao enviar pacote
}
void EnviaSmsM2(TipoRotina *RotinaEnvia,TipoRotina *RotinaGeraTelef,U8 RotinaSeSucesso)
{
	SCISendString((CAR8*)At_EnviaSmsP1_M2);        //envia o comando para abertura do terminal
    RotinaGeraTelef();
	SCISendString((CAR8*)At_EnviaSmsP2_M2);        //envia o comando para abertura do terminal
	GsmDelayRot(Mili500,GsmSendSms);   //gera um delay para a verifica��o de abertura do terminal
	GprsRotinaEnviar=RotinaEnvia;           //carrega o endere�o do pacote a ser enviado
	GprsRotOkPac=RotinaSeSucesso;           //rotina se sucesso ao enviar pacote
}
//fim do arquivo
