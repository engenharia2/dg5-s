//TrataPgm.c
#include "TrataPgm.h"
#include "derivative.h"
#include "EmulaEEprom.h"

U16 TimerPgm;

void InitPgm(void)
{
    DIR_REL_PGM=1;
    CR1_REL_PGM=1;
    REL_PGM=0;
    TimerPgm=0;
}

//a cada 100ms no loop principal
//controla os perif�ricos de sa�da
void ThreadPgm(void)
{
    //controla a pgm
    if(TimerPgm)
    {
        TimerPgm--;
        if(!TimerPgm)
        {
            REL_PGM=0;    
        }
    }
}

void AcionaPgm(void)
{
    U8 ModoLido;
    U16 TempoLido;
    U8 ModoMoni;

    //busca se � pulso ou reten��o da mem�ria
    //se for pulso conta o tempo
    //efetua a a��o
    TempoLido=ReadEEpromInt(eTempoPgm);
    ModoLido=ReadEEprom(eModoPgm);
    ModoMoni=ReadEEprom(eModoMoni);
    
    if(ModoLido==PgmModoRet
    && ModoMoni==MoniModoOff)
    {
        //reten��o
        TimerPgm=0;
        if(REL_PGM)
        {
            REL_PGM=0;
        }else{
            REL_PGM=1;
        }
    }else{
        //pulso
        REL_PGM=1;
        TimerPgm=TempoLido*10;
    }
}

void SetModoPgm(U8 Modo)
{
    TimerPgm=0;
    REL_PGM=0;
    WriteEEprom(eModoPgm,Modo);
}

void SetTempoPgm(U16 Tempo)
{
    WriteEEpromInt(eTempoPgm,Tempo);//em segundos
}

void SetModoMoni(U8 Modo)
{
    WriteEEprom(eModoMoni,Modo);
}
//fim do arquivo
