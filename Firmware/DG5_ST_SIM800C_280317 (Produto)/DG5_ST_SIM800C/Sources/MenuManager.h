#ifndef MENU_MANAGER_H
#define MENU_MANAGER_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */
#include "MenuNiveis.h"

#define LimBufTec 25//quantidade real de d�gitos que s�o armazenados no buffer

typedef union
{
    U8 Byte;
    struct
    {
        U8 lsb : 4;
        U8 hsb : 4;
    }Nibble;
}DigitBuf;

//estrutura que define os estados do teclado
struct TecladoState{
	U8 Apont;//armazena a quantidade de d�gitos de entrada
	U8 Digit;//armazena o d�gito atual
	DigitBuf Buf[LimBufTec];//buffer armazena as teclas de entrada do teclado
	U8 Nivel;//nivel de setup do teclado
	U16 Timer;//tempo de encerramento da programa��o
	U8 Beep;//gera��o de beeps
};

typedef struct TecladoState TEC;
extern TEC Tec;
#define TeC Tec

//cria a defini��o do tipo de rotina que � chamada pelo sistema de menus
typedef void (TipoMenus)(void);

struct MenusSistema{
	U8 Codigo;
	TipoMenus *Rotinas;
	TipoMenus *PreRot;
};
typedef struct MenusSistema MENUS;

#define NivNone     0//n�o possui menu para acesso a essa tarefa

//implementado no arquivo dos n�veis
extern const MENUS MenuV4[NumMenus];

void MPreNone(void);
void MNivelSelect(void);
void GoMeSelNiv(void);
void GoMeRaiz(void);
void UpdateMenu(void);

#define MNone MPreNone
//tempo limite no setup sem pressionar nenhuma tecla
#define TimeoutSetup    300//30 segundos

//menu com implementa��o obrigat�ria fixo do sistema
#define MeRaiz      0
#define MeSelecNiv  1

void InitMenu(void);
/*
    //a cada 100ms no loop principal
    (void)InstalaRotina(&MenuTrataTimer,100,MODO_LOOP_TEMPO);
*/
void MenuTrataTimer(void);
void TecBufAddDigito(U8 Digito);
void MenuTrataDigito(U8 Digito);

//para gera��o dos beeps
#define Beep1Curto      0x01//tecla pressionada
#define Beep2Curtos     0x02//status de ok
#define Beep1Longo      0x81//status de erro
#define GetNumBeeps(b)  (b&0x7F)
#define GetTipoBeep(b)  (b&0x80)

#endif
//fim do arquivo
