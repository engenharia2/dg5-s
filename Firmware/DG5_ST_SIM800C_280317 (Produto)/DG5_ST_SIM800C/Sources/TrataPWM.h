#ifndef TRATA_PWM_H
#define TRATA_PWM_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

void InitPWM(void);
void StartPWM(U16 FreqHz);
void StopPWM(void);

#endif
//fim do arquivo
