#ifndef TRATA_DSP_H
#define TRATA_DSP_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

#define ENT_DSP     PC_IDR_IDR5
#define DIR_ENT_DSP PC_DDR_DDR5
#define CR1_ENT_DSP PC_CR1_C15

void InitDsp(void);

/*
    //tarefa a cada 10ms no loop principal
    (void)InstalaRotina(&TarefaTrataDsp,10,MODO_LOOP_TEMPO);
*/
void TarefaTrataDsp(void);

U8 StatusDsp(void);

#endif
//fim do arquivo
