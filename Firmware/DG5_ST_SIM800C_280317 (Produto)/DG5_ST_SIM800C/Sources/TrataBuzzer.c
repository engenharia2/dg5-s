//TrataBuzzer.c

#include "TrataBuzzer.h"
#include "derivative.h"         /* include peripheral declarations                  */

U8 ContBeep=0;//n�mero de beep que devem ser gerados
U8 TipoBeep;//0 curto 1 longo
U8 TimerBeep=0;
U8 TimerMudo=0;//intervalo entre beep

void InitBuzzer(void)
{
    //init do buzzer
    BEEP_CSR_BEEPEN=0;
    BEEP_CSR_BEEPSEL=2;
    BEEP_CSR_BEEPDIV=14;//16;//128/(2x16)=4kHz
}

//a cada 10ms na interrup��o
void MenuTrataBeep(void)
{
    if(TimerMudo)
    {
        TimerMudo--;
    }
    else if(!TimerBeep)//caso n�o esteja gerando um beep
    {
        if(ContBeep)
        {
            ContBeep--;
            if(TipoBeep)TimerBeep=TBeepLongo;//beep longo
            else TimerBeep=TBeepCurto;//beep curto
            //inicia o beep
            BEEP_CSR_BEEPEN=1;
        }
    }
    else//caso esteja gerando um beep
    {
        TimerBeep--;//decrementa o tempo
        if(!TimerBeep)//quando atingir o timeout
        {
            //encerra o beep
            BEEP_CSR_BEEPEN=0;
            //aguarda o tempo de intervalo entre beep para gerar o pr�ximo
            if(ContBeep>9)TimerMudo=TBeepMudoM10;
            else TimerMudo=TBeepMudo;
        }
    }
}
//fim do arquivo
