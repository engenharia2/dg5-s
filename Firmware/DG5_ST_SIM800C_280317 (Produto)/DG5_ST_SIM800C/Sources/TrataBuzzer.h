#ifndef TRATA_BUZZER_H
#define TRATA_BUZZER_H

#include "Tipos_v2.h"           /* definição dos tipos de variáveis                 */

#define TBeepLongo      60//600ms beep longo
#define TBeepCurto      10//100ms beep curto
#define TBeepMudo       10//100ms intervalo entre beep quando menos que 10 beep
#define TBeepMudoM10    30//300ms intervalo entre beep quando igual ou mais que 10 beep

extern U8 ContBeep;
extern U8 TipoBeep;

/*
    //tarefa na interrupção a cada 10ms
    (void)InstalaRotina(&MenuTrataBeep,10,MODO_INTERRUPT);
*/
void MenuTrataBeep(void);
void InitBuzzer(void);

#endif
//fim do arquivo
