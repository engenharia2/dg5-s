#ifndef STATE_MANAGER_H
#define STATE_MANAGER_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

typedef void (TipoRotina)(void);

extern TipoRotina *GprsRotinaEnviar;
extern U8 EstadoGsm;
extern U8 *GsmCmdAnt;
extern U8 GsmCmdRec;
//extern U8 GsmCmdRec2;
extern U8 GprsRotOkPac;

#define GsmBootPos		0//posi��o de inicializa��o
#define GsmDelayPos		1//posi��o da rotina de delay

void GsmInitTask(void);
void GsmTrataDelay(void);
void GsmDelayRot(U16 Tempo,U8 Retorno);
void EnviaComandoGsm(const CAR8 *ComandoEnviar,const CAR8 *RespostaEsperada,U16 TempoLimiteEspera,U8 RotinaSeSucesso,U8 RotinaSeTimeout);
void EsperaComandoGsm(const CAR8 *RespostaEsperada,U16 TempoLimiteEspera,U8 RotinaSeSucesso,U8 RotinaSeTimeout);
void EsperaComandoGsmArgs(const CAR8 *RespostaEsperada,U16 TempoLimiteEspera,U8 RotinaSeTimeout,
const CAR8 *Argumento1,const CAR8 *Argumento2,const CAR8 *Argumento3,
U8 RotinaSeSucesso1,U8 RotinaSeSucesso2,U8 RotinaSeSucesso3,U8 RotinaSeErrArg);
void EnviaSms(TipoRotina *RotinaEnvia,TipoRotina *RotinaGeraTelef,U8 RotinaSeSucesso);
void RotProcSimplGsm(CAR8 *Param,U8 *Apont);
//void RotProcSimplGsm2(CAR8 *Param,U8 *Apont);
void EnviaSmsM2(TipoRotina *RotinaEnvia,TipoRotina *RotinaGeraTelef,U8 RotinaSeSucesso);

#endif
//fim do arquivo
