#ifndef EMULA_EEPROM_H
#define EMULA_EEPROM_H

#include "Tipos_v2.h"

void InitFlash(void);

U8 ReadEEprom(U16 Addr);

void WriteEEprom(U16 Addr,U8 Valor);

void WriteEEpromInt(U16 Addr, U16 Valor);

U16 ReadEEpromInt(U16 Addr);

#endif
