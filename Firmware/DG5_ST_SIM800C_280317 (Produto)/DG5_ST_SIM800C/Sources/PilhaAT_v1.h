#ifndef PILHAAT_H
#define PILHAAT_H

//Pilha_AT.h
//Joel Langer
//Todas as configura��es permitidas est�o aqui
//Ultima Reviz�o 06/02/2009
//Vers�o 1

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

//cria a defini��o do tipo de rotina que deve ser criada para ser instalada no sistema
//deve receber a entrada do endere�o do buffer de parametros e do apontador de buffer de parametros
typedef void (TipComandos)(CAR8 *Param,U8 *Apont);

#define TamanDoBuf 100 //capacidade em bytes do buffer serial
#define TamanDoBufArgs 100 //capacidade em bytes do buffer de argumentos da recep��o serial
#define MaxComds 15 //limita a quantidade de comandos para n�o estrapolar a ram
#define TamMaxArg 15//fun��o de compara��o de argumento string
#define ECOAR_CARACTERES FALSE//habilita e desabilia o eco no terminal

//incluimos as fun��es de transmiss�o serial
//translada o nome para as fun��es que s�o chamadas pela api
#define TermGetByte() SCIGetByte()
#define TermSendByte(d) SCISendByte(d)

//define o vetor de interrup��o serial instalada no tratamento AT
//#define IntRecSerial(void) __interrupt void isrVscirx(void)
//#define IntRecSerial(void) void interrupt VectorNumber_Vscirx Scirx(void)

//prot�tipo de fun��es
void InitTerminal(void);
U8 TermInsertComand(CAR8 * Arg,TipComandos * Comando);
U8 TermDelComand(CAR8 * Arg);
void TermClearAllAtCommands(void);
U8 GetNumArgument(CAR8 *Param,U8 *Apont);
U8 ArgumentToInt(U8 Argum,U16 *Valor,CAR8 *Param,U8 *Apont);
U8 CompString(CAR8 *a,CAR8 *b);
U8 ArgumentToString(U8 Argum,CAR8 *Matriz,U8 *QunatRec,U8 Quanto,CAR8 *Param,U8 *Apont);
U8 ComparaArgumentString(CAR8 *Param,U8 *Apont,U8 Argumento,CAR8 *Comando);

//chamada simplificada para instala��o e remo��o de comandos no sistema
#define AddCmd(arg1,arg2) (void)TermInsertComand((CAR8*)arg1,arg2)
#define RemCmd(arg1) (void)TermDelComand((CAR8*)arg1)


/*Aten��o!! N�o esque�a de chamar a rotina de inicializa��o do terminal
InitTerminal();
*/
#endif
//fim do arquivo