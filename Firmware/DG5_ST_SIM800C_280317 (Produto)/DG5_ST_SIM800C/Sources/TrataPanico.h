#ifndef TRATA_PANICO_H
#define TRATA_PANICO_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

#define ENT_PANICO     PB_IDR_IDR4
#define DIR_ENT_PANICO PB_DDR_DDR4
#define CR1_ENT_PANICO PB_CR1_C14

void InitPanico(void);

/*
    //tarefa a cada 10ms no loop principal
    (void)InstalaRotina(&TarefaTrataPanico,10,MODO_LOOP_TEMPO);
*/
void TarefaTrataPanico(void);

U8 StatusPanico(void);

#endif
//fim do arquivo
