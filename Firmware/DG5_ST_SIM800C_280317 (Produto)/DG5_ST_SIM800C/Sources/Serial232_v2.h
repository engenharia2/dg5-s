#ifndef SERIAL_232_H
#define SERIAL_232_H
//Lib para comunica��o serial
//Joel Langer
//Vers�o 2
#include "Tipos_v2.h" //defini��o dos tipos de vari�veis

//#define IntTxSerial(void) __interrupt void isrVscitx(void)
//#define IntTxSerial(void) void interrupt VectorNumber_Vscitx Scitx(void)
#define TamanBufTx 80 //capacidade em bytes do buffer de transmiss�o

//#define V2_D         SCID
//#define V2_S1_RDRF   SCIS1_RDRF
//#define V2_S1_TDRE   SCIS1_TDRE
//#define V2_C2_TCIE   SCIC2_TCIE

CAR8 SCIGetByte(void);
void InitTxSerial(void);
void SCISendByte(CAR8 Valor);
void SCISendString(CAR8 * c);
void SCISendBin(U8 Valor);
void SCISendHex(U8 Valor);
void SCISendInt(U16 Valor);
void SCISendLong(U32 Valor);

#endif
//fim do arquivo
