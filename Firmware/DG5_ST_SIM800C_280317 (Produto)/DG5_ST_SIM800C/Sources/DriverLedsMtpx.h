#ifndef DRIVER_LEDS_MTPX_H
#define DRIVER_LEDS_MTPX_H

#include "Tipos_v2.h"

#define LED_MUX_1       PD_ODR_ODR1
#define DIR_LED_MUX_1   PD_DDR_DDR1
#define CR1_LED_MUX_1   PD_CR1_C11

#define LED_MUX_2       PC_ODR_ODR6
#define DIR_LED_MUX_2   PC_DDR_DDR6
#define CR1_LED_MUX_2   PC_CR1_C16

#define LED_MUX_3       PC_ODR_ODR7
#define DIR_LED_MUX_3   PC_DDR_DDR7
#define CR1_LED_MUX_3   PC_CR1_C17

typedef union
{
    U8 u8Bits;
    struct
    {
        U8 led0 :1;
        U8 led1 :1;
        U8 led2 :1;
        U8 led3 :1;
        U8      :1;
        U8      :1;
        U8      :1;
        U8      :1;
    }sBits;
}FlagLeds;

extern FlagLeds Leds;

void InitLedsMtpx(void);
void UpdateLedsMtpx(void);

#endif
