#include "derivative.h" /* include peripheral declarations */
#include "TrataTeclado.h"
#include "LedManager_v1.h"
#include "TickTimer.h"
#include "TrataBuzzer.h"
#include "CopProcess.h"
#include "TrataPgm.h"
#include "TrataDsp.h"
#include "EmulaEEprom.h"
#include "Serial232_v2.h"
#include "PilhaAT_v1.h"
#include "ATStateManager_v1.h"
#include "GerenciadorGprs_v1.h"
#include "MenuManager.h"
#include "DiscadorManager.h"
#include "TrataPanico.h"
#include "TrataMoni.h"
#include "DriverLedsMtpx.h"
#include "TrataPWM.h"

/*
Mapa da mem�ria
AddrDisc    0
eTempoPgm   55 //55 e 56 int
eModoPgm    57 //57 char
eModoSms    58 //58 char
eModoMoni   59 //59 char

R00 10/01/17
revis�o inicial
Compliado com IAR Embedded Workbench for STMicroelectronics STM8 version 2.20.2

Criar inicializa��o padr�o da EEPROM atrav�s de byte de check

Se der erro no envio de sms de resposta de pgm deve ir para online n�o discagem!
Copiar rotina que aguarda sinal presente da atualiza��o feita em simcom?
*/

//em TRUE n�o protege a mem�ria permitindo depura��o
#define DEBUG_MODE FALSE

#if DEBUG_MODE==TRUE
#warning "Modo de depura��o"
#endif

//COP a 1024ms
//CPU a 16MHz
//Tick no TIM4 (init pr�prio)
//PWM no TIM1 (init pr�prio)
void MCU_Init(void)
{
    __disable_interrupt();
    
    //Fmaster = 16MHz
    //HSI
    CLK_CKDIVR=(MASK_CLK_CKDIVR_HSIDIV&0);//prescaler = 1

    #if DEBUG_MODE==FALSE
    //Independent watchdog
    IWDG_KR=KEY_ENABLE;//independent watchdog is started
    IWDG_KR=KEY_ACCESS;//desabilita a prote��o de escrita dos registradores IWDG_PR e IWDG_RLR
    IWDG_PR_PR=0x06;//prescaler=256
    IWDG_RLR=0xFF;//reload
    IWDG_KR=KEY_REFRESH;//habilita a prote��o de escrita dos registradores IWDG_PR e IWDG_RLR e faz o refresh
    #endif
}

TickS T10ms;
TickS T100ms;

//inicializa valores padr�es
void InitEeprom(void)
{

}

void main(void)
{
    U8 SWIMDelayedStartup=5;//500ms para o programador de produ��o
    //Inicializa perif�ricos 
    MCU_Init();//Inicializa a CPU
    InitFlash();
    //Inicializa aplica��o
    TickClockInit();//base de tempo
    InitBuzzer();
    InitLedsMtpx();
    InitLeds();
    InitTeclado();
    InitMenu();
    InitPgm();
    InitDsp();
    InitPanico();
    InitMoni();

    DIR_PWR_KEY_GPRS=1;//power key
    CR1_PWR_KEY_GPRS=1;
    PWR_KEY_GPRS=0;

    DIR_MOS_GPRS=1;//mosfet alimenta m�dulo gsm
    CR1_MOS_GPRS=1;
    MOS_GPRS=0;

    InitEeprom();
    InitTerminal();
    GsmInitTask();
    //inicializa��o da serial de comunica��o com o GPRS
    InitTxSerial();
    InitPWM();
    
    TickSet(10,&T10ms);
    TickSet(100,&T100ms);

    __enable_interrupt();
    
    #if DEBUG_MODE==TRUE
    {
    U8 TelTest[]={0x09,0x99,0x52,0x82,0x17};
    SetMasterReset();
    DiscProgramTelef(0,TelTest,9);
    }
    #endif
    
    for(;;)
    {
        if(TickExpired(&T10ms))
        {
            TickReset(&T10ms);
            TarefaTrataTeclado();
            UpdateLedsMtpx();
            MenuTrataBeep();
            TarefaTrataDsp();
            TarefaTrataPanico();
            TarefaTrataMoni();
            CopProcess();
        }
        if(TickExpired(&T100ms))
        {
            TickReset(&T100ms);
            if(SWIMDelayedStartup)
            {
                SWIMDelayedStartup--;
                if(!SWIMDelayedStartup)
                {
                    PD_DDR_DDR1=1;
                    PD_CR1_C11=1;
                    PD_ODR_ODR1=0;
                }
            }
            MenuTrataTimer();
            ThreadPilhaAtMaster();
            ThreadPiscaLeds();
            ThreadPgm();
            CopPeriodic();
        }
    }
}
