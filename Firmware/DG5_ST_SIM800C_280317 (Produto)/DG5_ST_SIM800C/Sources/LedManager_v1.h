#ifndef LED_MANAGER_V1_H
#define LED_MANAGER_V1_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

#define NumLedsSist 4

//Estado dos LEDS
//(0 apagado) (1 aceso) (2 piscando alternado) (3 piscando r�pido)
//deve ser instalada para rodar a cada 100ms
#define LedOff      0
#define LedOn       1
#define LedAlterna  2
#define LedFlash    3

typedef union
{
    U8 Byte;
    struct
    {
        U8 Modo     :2;//1 ligado 0 desligado
        U8 Estado   :1;//0 desligado 1 ativo
    }Bits;
}TipoLeds;

extern TipoLeds LedsStat[NumLedsSist];

#define LedSinalInf     LedsStat[0].Bits.Modo
#define LedSinalSup     LedsStat[1].Bits.Modo
#define LedErrGsm       LedsStat[2].Bits.Modo
#define LedDisc         LedsStat[3].Bits.Modo

/*
    Deve ser instalada na interrup��o de tempo
    (void)InstalaRotina(&ThreadPiscaLeds,100,MODO_INTERRUPT);
*/
void ThreadPiscaLeds(void);
void InitLeds(void);

#endif
//fim do arquivo
