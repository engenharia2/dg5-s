#ifndef GPRS_MANAGER_H
#define GPRS_MANAGER_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

#define eModoSms    58 //58 char

#define PWR_KEY_GPRS        PA_ODR_ODR1
#define DIR_PWR_KEY_GPRS    PA_DDR_DDR1
#define CR1_PWR_KEY_GPRS    PA_CR1_C11

#define MOS_GPRS            PA_ODR_ODR3
#define DIR_MOS_GPRS        PA_DDR_DDR3
#define CR1_MOS_GPRS        PA_CR1_C13

//defini��o para os tempos
#define Mili500 5

#define Seg1 10
#define Seg2 20
#define Seg3 30
#define Seg5 50
#define Seg10 100
#define Seg15 150

#define Min1 600
#define Min2 1200

//estados do tratamento gsm
#define GsmOffModulo                GsmBootPos	//0
#define GsmDelay                    GsmDelayPos //1
#define GsmSendSms                  2//2
#define GsmFalhaComu                3
#define GsmEndOffModulo             4
#define GsmCheckOffModulo           5
#define GmsPwrOnModulo              6
#define GsmEndOnModulo              7
#define GsmCheckOnModulo            8
#define GsmHardOfModulo             9
#define GsmHardOnModulo             10
#define GsmAutoBaud                 11
#define GsmSetupEcho                12
#define GsmSetupModText             13
#define GsmWaitCallReady            14
#define GsmOnline                   15
#define GsmDelayProxSms             16
#define GsmProxSms                  17
#define GsmCancellSms               18
#define GsmDelayProxDisc            19
#define GsmProxDisc                 20
#define GsmEmDisc                   21
#define GsmSetupColp                22
#define GsmWaitCon                  23
#define GmsEndDisc                  24
#define GsmClearSms                 25
#define GsmSendSmsPgm               26
#define GsmWaitSmsReady             27
#define GsmWaitCheckCpin            28
#define GsmCheckCpin                29
#define GsmWaitCpinReady            30

#define FreqDispHi      1000
#define FreqDispLo      800

extern const CAR8 At_EnviaSmsP1[];
extern const CAR8 At_EnviaSmsP2[];
extern const CAR8 At_EnviaSmsP1_M2[];
extern const CAR8 At_EnviaSmsP2_M2[];
extern const CAR8 cmd_CMS[];
extern const CAR8 cmd_ERROR[];

extern U8 TestDisc;

#define SmsModoOn   1
#define SmsModoOff  0
#define SmsModoOnly 2

void SetModoSms(U8 Modo);

void ThreadPilhaAtMaster(void);
/*
    //Pilha Gsm a cada 100ms no loop principal
    (void)InstalaRotina(&ThreadPilhaAtMaster,100,MODO_LOOP_TEMPO);
*/

#endif
//fim do arquivo
