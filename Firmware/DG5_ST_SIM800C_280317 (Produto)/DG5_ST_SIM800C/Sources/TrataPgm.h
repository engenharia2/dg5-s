#ifndef TRATA_PGM_H
#define TRATA_PGM_H

#include "Tipos_v2.h"           /* defini��o dos tipos de vari�veis                 */

#define eTempoPgm   55 //55 e 56 int
#define eModoPgm    57 //57 char
#define eModoMoni   59 //59 char

//rel� da PGM
#define REL_PGM     PA_ODR_ODR2
#define DIR_REL_PGM PA_DDR_DDR2
#define CR1_REL_PGM PA_CR1_C12

void InitPgm(void);

void ThreadPgm(void);
/*
    //Controle dos perif�ricos a cada 100ms no loop principal
    (void)InstalaRotina(&ThreadPgm,100,MODO_LOOP_TEMPO);
*/

void AcionaPgm(void);

#define PgmModoRet  1
#define PgmModoPls  0

#define MoniModoOn  1
#define MoniModoOff 0

void SetModoPgm(U8 Modo);
void SetTempoPgm(U16 Tempo);
void SetModoMoni(U8 Modo);

#endif
//fim do arquivo
