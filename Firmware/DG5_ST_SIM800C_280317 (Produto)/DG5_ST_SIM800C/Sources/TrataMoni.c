//TrataMoni.c

#include "TrataMoni.h"
#include "derivative.h"         /* include peripheral declarations                  */

#define TempoMoniRest    20//200ms para restaurar o status
#define TempoMoniAtiv    200//2000ms para definir como acionado

struct
{
    U8 Stat;
    U16 Timer;
}SensorMoni;

//retorna 1 se estiver acionado 0 se n�o
U8 StatusMoni(void)
{
    return(SensorMoni.Stat);
}

void InitMoni(void)
{
    DIR_ENT_MONI=0;
    CR1_ENT_MONI=0;

    SensorMoni.Stat=0;
    SensorMoni.Timer=0;
}

//instalada para ser executada a cada 10ms no loop principal
//trata o tempo para validar o acionamento da Moni
void TarefaTrataMoni(void)
{
    if(ENT_MONI)//acionado
    {
        if(!SensorMoni.Stat)//flag ainda n�o foi setado
        {
            SensorMoni.Timer++;//incrementa o timer
            if(SensorMoni.Timer>=TempoMoniAtiv)
            {
                SensorMoni.Timer=0;//encerra o timer
                SensorMoni.Stat=1;//seta o flag
            }
        }
        else//flag j� foi setado
        {
            SensorMoni.Timer=0;//apaga o timer    
        }
    }
    else//n�o acionado
    {
        if(SensorMoni.Stat)//flag ainda n�o foi apagado
        {
            SensorMoni.Timer++;//incrementa o timer
            if(SensorMoni.Timer>=TempoMoniRest)
            {
                SensorMoni.Timer=0;//encerra o timer
                SensorMoni.Stat=0;//apaga o flag
            }
        }
        else//flag j� foi apagado
        {
            SensorMoni.Timer=0;//apaga o timer    
        }
    }
}
//fim do arquivo
